﻿namespace DAL.Entities
{
    public enum VehicleType
    {
        Bus,
        Tram,
        Train,
        Trolley
    }
}