﻿namespace DAL.Entities
{
    public enum TicketType
    {
        Child,
        Student,
        Senior,
        Basic
    }
}