﻿using System.Data.Entity;
using DAL.Entities;

namespace DAL
{
    public class TimetableDbContext : DbContext
    {
        public TimetableDbContext()
        {
            Database.SetInitializer(new TimetableDbInitializer());
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Seat>()
                .HasOptional(e => e.Ticket)
                .WithRequired(e => e.Seat).Map(e => e.MapKey());

            modelBuilder.Entity<Ticket>()
                .HasOptional(e => e.Form)
                .WithRequired(e => e.Ticket).Map(e => e.MapKey());
        }

        public DbSet<Company> Companies { get; set; }

        public DbSet<Discount> Discounts { get; set; }

        public DbSet<Form> Forms { get; set; }

        public DbSet<Link> Links { get; set; }

        public DbSet<PriceCatalogue> PriceCatalogues { get; set; }

        public DbSet<Route> Routes { get; set; }

        public DbSet<TwoStopRoute> TwoStopRoutes { get; set; }

        public DbSet<Seat> Seats { get; set; }

        public DbSet<Stop> Stops { get; set; }

        public DbSet<Ticket> Tickets { get; set; }

        public DbSet<TimedStop> TimedStops { get; set; }

        public DbSet<User> Users { get; set; }

        public DbSet<Vehicle> Vehicles { get; set; }
    }
}