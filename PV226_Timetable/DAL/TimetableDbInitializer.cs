﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using DAL.Entities;

namespace DAL
{
    public class TimetableDbInitializer : DropCreateDatabaseAlways<TimetableDbContext>
    {
        public override void InitializeDatabase(TimetableDbContext context)
        {
            base.InitializeDatabase(context);

            Discount childDiscount = new Discount()
            {
                DiscountPercentage = 30,
                TicketType = TicketType.Child
            };
            context.Discounts.Add(childDiscount);

            Discount studentDiscount = new Discount()
            {
                DiscountPercentage = 15,
                TicketType = TicketType.Student
            };
            context.Discounts.Add(studentDiscount);

            var discounts = new HashSet<Discount>();
            discounts.Add(childDiscount);
            discounts.Add(studentDiscount);

            var priceCatalogue = new PriceCatalogue()
            {
                Discounts = discounts,
                PricePerKm = 12,
            };
            context.PriceCatalogues.Add(priceCatalogue);

            var anotherCatalogue = new PriceCatalogue()
            {
                Discounts = new HashSet<Discount>(),
                PricePerKm = 4,
            };

            context.PriceCatalogues.Add(anotherCatalogue);

            var skoda = new Company()
            {
                Name = "Skoda",
                IsTicketReturnAllowed = true,
                PriceCatalogue = priceCatalogue,
            };

            var renault = new Company()
            {
                Name = "Renault",
                IsTicketReturnAllowed = false,
                PriceCatalogue = anotherCatalogue,
            };

            context.Companies.Add(skoda);
            context.Companies.Add(renault);

            var tylova = new Stop() {Name = "Tylova", Town = "Brno" };
            context.Stops.Add(tylova);

            var tylova30Sec = new TimedStop() {Stop = tylova, StopTime = new TimeSpan(0, 0, 30),};
            context.TimedStops.Add(tylova30Sec);

            var semilasso = new Stop() { Name = "Semilasso", Town = "Brno" };
            context.Stops.Add(semilasso);

            var semilasso1Min = new TimedStop() { Stop = semilasso, StopTime = new TimeSpan(0, 1, 0), };
            context.TimedStops.Add(semilasso1Min);

            var husitska = new Stop() { Name = "Husitska", Town = "Brno" };
            context.Stops.Add(semilasso);

            var husitska20sec = new TimedStop() { Stop = husitska, StopTime = new TimeSpan(0, 0, 20), };
            context.TimedStops.Add(semilasso1Min);

            // From Husitska to Tylova
            var semilassoHusitskaTwoStop = new TwoStopRoute()
            {
                LeaveStop = husitska20sec,
                ArriveStop = semilasso1Min,
                LengthInKm = 0.75m,
                TravelTime = new TimeSpan(0, 1, 0),
            };
            context.TwoStopRoutes.Add(semilassoHusitskaTwoStop);

            var tylovaSemilassoTwoStop = new TwoStopRoute()
            {
                LeaveStop = semilasso1Min,
                ArriveStop = tylova30Sec,
                LengthInKm = 2.5m,
                TravelTime = new TimeSpan(0, 2, 30),
            };
            context.TwoStopRoutes.Add(tylovaSemilassoTwoStop);

            var stops = new List<TwoStopRoute>();
            stops.Add(tylovaSemilassoTwoStop);
            stops.Add(semilassoHusitskaTwoStop);

            var route = new Route() {TwoStopRoutes = stops,};
            context.Routes.Add(route);

            var tram = new Vehicle() {Capacity = 10, Company = skoda, VehicleType = VehicleType.Tram};
            context.Vehicles.Add(tram);

            var link = new Link()
            {
                LeaveTime = new DateTime(2017, 11, 24, 17, 35, 0),
                Delay = TimeSpan.Zero,
                Route = route,
                Vehicle = tram,
            };
            context.Links.Add(link);

            // From Tylova to Husitska
            var semilassoTylovaTwoStop = new TwoStopRoute()
            {
                LeaveStop = tylova30Sec,
                ArriveStop = semilasso1Min,
                LengthInKm = 2.5m,
                TravelTime = new TimeSpan(0, 2, 30),
            };
            context.TwoStopRoutes.Add(semilassoTylovaTwoStop);

            var husitskaSemilasoTwoStop = new TwoStopRoute()
            {
                LeaveStop = semilasso1Min,
                ArriveStop = husitska20sec,
                LengthInKm = 0.75m,
                TravelTime = new TimeSpan(0, 1, 0),
            };
            context.TwoStopRoutes.Add(husitskaSemilasoTwoStop);

            stops = new List<TwoStopRoute>();
            stops.Add(semilassoTylovaTwoStop);
            stops.Add(husitskaSemilasoTwoStop);

            route = new Route() { TwoStopRoutes = stops, };
            context.Routes.Add(route);

            link = new Link()
            {
                LeaveTime = new DateTime(2017, 10, 25, 10, 30, 0),
                Delay = new TimeSpan(0, 5, 0),
                Route = route,
                Vehicle = tram,
            };
            context.Links.Add(link);

            var seat1 = new Seat()
            {
                Link = link,
                Number = 1,
            };
            context.Seats.Add(seat1);
            var seat2 = new Seat()
            {
                Link = link,
                Number = 2,
            };
            context.Seats.Add(seat2);
            var seat3 = new Seat()
            {
                Link = link,
                Number = 3,
            };
            context.Seats.Add(seat3);

            var user = new User()
            {
                FirstName = "Marek",
                LastName = "Sabo",
                Email = "sabo@mail.muni.cz",
            };
            context.Users.Add(user);

            var ticket = new Ticket()
            {
                From = semilasso,
                To = tylova,
                IsCanceled = false,
                IsPayed = true,
                Price = 20.0m,
                User = user,
                Seat = seat1,
                TimeCreated = DateTime.Now
            };
            context.Tickets.Add(ticket);

            context.SaveChanges();
        }
    }
}