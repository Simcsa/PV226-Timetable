﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Riganti.Utils.Infrastructure.Core;

namespace DAL.Entities
{
    public class Vehicle : IEntity<int>
    {
        [Key]
        public int ID { get; set; }

        [Required]
        public VehicleType VehicleType { get; set; }

        [Required]
        public int Capacity { get; set; }

        public virtual List<Link> Link { get; set; }

        [Required]
        public virtual Company Company { get; set; }
    }
}