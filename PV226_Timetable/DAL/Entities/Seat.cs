﻿using System.ComponentModel.DataAnnotations;
using Riganti.Utils.Infrastructure.Core;

namespace DAL.Entities
{
    public class Seat : IEntity<int>
    {
        [Key]
        public int ID { get; set; }

        [Required]
        public int Number { get; set; }

        public virtual Ticket Ticket { get; set; }

        [Required]
        public virtual Link Link { get; set; }
    }
}