﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Riganti.Utils.Infrastructure.Core;

namespace DAL.Entities
{
    public class Link : IEntity<int>
    {
        [Key]
        public int ID { get; set; }

        [Required]
        public DateTime LeaveTime { get; set; }

        public TimeSpan Delay { get; set; }

        [Required]
        public Route Route { get; set; }

        [Required]
        public virtual Vehicle Vehicle { get; set; }

        public virtual List<Seat> Seats { get; set; }
    }
}