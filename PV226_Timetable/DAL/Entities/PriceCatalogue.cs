﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Riganti.Utils.Infrastructure.Core;

namespace DAL.Entities
{
    public class PriceCatalogue : IEntity<int>
    {
        [Key]
        public int ID { get; set; }

        [Required]
        public decimal PricePerKm { get; set; }

        public HashSet<Discount> Discounts { get; set; }
    }
}