﻿using Riganti.Utils.Infrastructure.Core;
using System;
using System.ComponentModel.DataAnnotations;

namespace DAL.Entities
{
    public class TimedStop : IEntity<int>
    {
        [Key]
        public int ID { get; set; }

        [Required]
        public TimeSpan StopTime { get; set; }

        [Required]
        public virtual Stop Stop { get; set; }

        public override string ToString()
        {
            return $"{nameof(Stop)} {Stop} with stop time {StopTime}";
        }
    }
}