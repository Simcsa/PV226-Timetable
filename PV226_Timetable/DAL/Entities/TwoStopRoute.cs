﻿using System;
using System.ComponentModel.DataAnnotations;
using Riganti.Utils.Infrastructure.Core;
using System.ComponentModel.DataAnnotations.Schema;

namespace DAL.Entities
{
    public class TwoStopRoute : IEntity<int>
    {
        [Key]
        public int ID { get; set; }

        public int Order { get; set; }

        [Required]
        public TimeSpan TravelTime { get; set; }

        [Required]
        public decimal LengthInKm { get; set; }

        public virtual TimedStop LeaveStop { get; set; }

        public virtual TimedStop ArriveStop { get; set; }

        public virtual Route Route { get; set; }

        public override string ToString()
        {
            return $"From {LeaveStop} to {ArriveStop}, {nameof(TravelTime)}: {TravelTime}, {LengthInKm} km";
        }
    }
}