﻿using System.ComponentModel.DataAnnotations;
using Riganti.Utils.Infrastructure.Core;

namespace DAL.Entities
{
    public class Discount : IEntity<int>
    {
        [Key]
        public int ID { get; set; }

        [Required]
        public TicketType TicketType { get; set; }

        [Required]
        [Range(0, 100)]
        public int DiscountPercentage { get; set; }
    }
}