﻿using System.ComponentModel.DataAnnotations;
using Riganti.Utils.Infrastructure.Core;

namespace DAL.Entities
{
    public class Form : IEntity<int>
    {
        [Key]
        public int ID { get; set; }

        [Required]
        [Range(1, 10)]
        public int Rating { get; set; }

        public string Comment { get; set; }
        
        public virtual Ticket Ticket { get; set; }
    }
}