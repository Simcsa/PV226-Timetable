﻿using System;
using System.ComponentModel.DataAnnotations;

using Riganti.Utils.Infrastructure.Core;

namespace DAL.Entities
{
    public class Ticket : IEntity<int>
    {
        [Key]
        public int ID { get; set; }

        public decimal Price { get; set; }

        public DateTime TimeCreated { get; set; }

        public bool IsPayed { get; set; }

        public bool IsCanceled { get; set; }

        public virtual Form Form { get; set; }

        [Required]
        public virtual User User { get; set; }

        public virtual Seat Seat { get; set; }

        public virtual Stop From { get; set; }

        public virtual Stop To { get; set; }
    }
}