﻿using System.ComponentModel.DataAnnotations;
using Riganti.Utils.Infrastructure.Core;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace DAL.Entities
{
    public class Stop : IEntity<int>
    {
        [Key]
        public int ID { get; set; }

        [Required]
        public string Name { get; set; }

        public string Town { get; set; }

        [JsonIgnore]
        public virtual List<Photo> Photos { get; set; }

        public override string ToString()
        {
            return $"{Name}" + (string.IsNullOrEmpty(Town) ? "" : ", {nameof(Town)}: {Town}");
        }
    }
}