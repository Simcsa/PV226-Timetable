﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Riganti.Utils.Infrastructure.Core;

namespace DAL.Entities
{
    public class Company : IEntity<int>
    {
        [Key]
        public int ID { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public bool IsTicketReturnAllowed { get; set; }

        [Required]
        public PriceCatalogue PriceCatalogue { get; set; }

        public virtual List<Vehicle> Vehicles { get; set; }
    }
}