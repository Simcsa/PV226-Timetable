﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Riganti.Utils.Infrastructure.Core;

namespace DAL.Entities
{
    public class Route : IEntity<int>
    {
        [Key]
        public int ID { get; set; }

        public virtual List<TwoStopRoute> TwoStopRoutes { get; set; }
    }
}