﻿using Riganti.Utils.Infrastructure.Core;
using System.ComponentModel.DataAnnotations;

namespace DAL.Entities
{
    public class Photo : IEntity<int>
    {
        [Key]
        public int ID { get; set; }

        public byte[] Data { get; set; }
    }
}
