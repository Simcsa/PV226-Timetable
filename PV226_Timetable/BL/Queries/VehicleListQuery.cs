﻿using System.Linq;
using AutoMapper.QueryableExtensions;
using BL.AppInfrastructure;
using BL.DTOs.Filters;
using BL.DTOs.Vehicles;
using DAL.Entities;
using Riganti.Utils.Infrastructure.Core;

namespace BL.Queries
{
    public class VehicleListQuery : AppQuery<VehicleDTO>
    {
        public VehicleFilter Filter { get; set; }

        public VehicleListQuery(IUnitOfWorkProvider provider) : base(provider) { }

        protected override IQueryable<VehicleDTO> GetQueryable()
        {
            IQueryable<Vehicle> query = Context.Vehicles;

            if (Filter?.Capacity != null)
            {
                query = query.Where(vehicle => vehicle.Capacity >= Filter.Capacity.Minimum && vehicle.Capacity <= Filter.Capacity.Maximum);
            }
            if (!string.IsNullOrEmpty(Filter?.CompanyName))
            {
                query = query.Where(vehicle => vehicle.Company.Name.ToLower().Contains(Filter.CompanyName.ToLower()));
            }
            if (Filter?.VehicleTypes != null)
            {
                query = query.Where(vehicle => Filter.VehicleTypes.Contains(vehicle.VehicleType));
            }

            return query.ProjectTo<VehicleDTO>();
        }
    }
}
