﻿using System.Linq;
using AutoMapper.QueryableExtensions;
using BL.AppInfrastructure;
using BL.DTOs.Filters;
using BL.DTOs.Users;
using DAL.Entities;
using Riganti.Utils.Infrastructure.Core;

namespace BL.Queries
{
    public class UserListQuery : AppQuery<UserDTO>
    {
        public UserFilter Filter { get; set; }

        public UserListQuery(IUnitOfWorkProvider provider) : base(provider) { }

        protected override IQueryable<UserDTO> GetQueryable()
        {
            IQueryable<User> query = Context.Users;

            if (!string.IsNullOrEmpty(Filter?.FirstName))
            {
                query = query.Where(user => user.FirstName.ToLower().Contains(Filter.FirstName.ToLower()));
            }
            if (!string.IsNullOrEmpty(Filter?.LastName))
            {
                query = query.Where(user => user.LastName.ToLower().Contains(Filter.LastName.ToLower()));
            }
            if (!string.IsNullOrEmpty(Filter?.Email))
            {
                query = query.Where(user => user.Email.ToLower().Contains(Filter.Email.ToLower()));
            }

            return query.ProjectTo<UserDTO>();
        }
    }
}
