﻿using System.Linq;
using AutoMapper.QueryableExtensions;
using BL.AppInfrastructure;
using BL.DTOs.Filters;
using BL.DTOs.Seats;
using DAL.Entities;
using Riganti.Utils.Infrastructure.Core;

namespace BL.Queries
{
    public class SeatListQuery : AppQuery<SeatDTO>
    {
        public SeatFilter Filter { get; set; }

        public SeatListQuery(IUnitOfWorkProvider provider) : base(provider) { }

        protected override IQueryable<SeatDTO> GetQueryable()
        {
            IQueryable<Seat> query = Context.Seats;

            if (Filter?.Number > 0)
            {
                query = query.Where(seat => seat.Number == Filter.Number);
            }
            if (Filter?.IsOccupied != null)
            {
                query = query.Where(seat => 
                    (Filter.IsOccupied.Value && seat.Ticket != null && !seat.Ticket.IsCanceled) 
                    || (!Filter.IsOccupied.Value && (seat.Ticket == null || seat.Ticket.IsCanceled)));
            } 
            if (Filter?.LinkId > 0)
            {
                query = query.Where(seat => seat.Link.ID == Filter.LinkId);
            }

            return query.ProjectTo<SeatDTO>();
        }
    }
}
