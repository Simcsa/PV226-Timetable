﻿using System.Linq;
using AutoMapper.QueryableExtensions;
using BL.AppInfrastructure;
using BL.DTOs.Filters;
using BL.DTOs.Links;
using DAL.Entities;
using Riganti.Utils.Infrastructure.Core;
using System;
using System.Data.Entity;

namespace BL.Queries
{
    public class LinkListQuery : AppQuery<LinkDTO>
    {
        public LinkFilter Filter { get; set; }

        public LinkListQuery(IUnitOfWorkProvider provider) : base(provider) { }

        protected override IQueryable<LinkDTO> GetQueryable()
        {
            IQueryable<Link> query = Context.Links;

            if (!string.IsNullOrEmpty(Filter?.LeaveStopName))
            {
                query = query.Where(link => link.Route.TwoStopRoutes.Count(route => route.LeaveStop.Stop.Name.ToLower().Contains(Filter.LeaveStopName)) > 0);
            }
            if (!string.IsNullOrEmpty(Filter?.ArriveStopName))
            {
                query = query.Where(link => link.Route.TwoStopRoutes.Count(route => route.ArriveStop.Stop.Name.ToLower().Contains(Filter.ArriveStopName)) > 0);
            }
            if (Filter?.LeaveTime != null)
            {
                query = query.Where(link => DbFunctions.AddMilliseconds(link.LeaveTime, DbFunctions.DiffMilliseconds(TimeSpan.Zero, link.Delay)) >= Filter.LeaveTime);
            }
            if (Filter?.VehicleTypes != null)
            {
                query = query.Where(link => Filter.VehicleTypes.Contains(link.Vehicle.VehicleType));
            }

            return query.ProjectTo<LinkDTO>();
        }
    }
}
