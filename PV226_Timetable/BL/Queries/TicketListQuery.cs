﻿using System.Linq;
using AutoMapper.QueryableExtensions;
using BL.AppInfrastructure;
using BL.DTOs.Filters;
using BL.DTOs.Tickets;
using DAL.Entities;
using Riganti.Utils.Infrastructure.Core;

namespace BL.Queries
{
    public class TicketListQuery : AppQuery<TicketDTO>
    {
        public TicketFilter Filter { get; set; }

        public TicketListQuery(IUnitOfWorkProvider provider) : base(provider) { }

        protected override IQueryable<TicketDTO> GetQueryable()
        {
            IQueryable<Ticket> query = Context.Tickets;

            if (Filter?.UserId > 0)
            {
                query = query.Where(ticket => ticket.User.ID == Filter.UserId);
            }
            if (Filter?.SeatNumber > 0)
            {
                query = query.Where(ticket => ticket.Seat.Number == Filter.SeatNumber);
            }
            if (Filter?.SeatId > 0)
            {
                query = query.Where(ticket => ticket.Seat.ID == Filter.SeatId);
            }
            if (Filter?.Price != null)
            {
                query = query.Where(ticket => ticket.Price >= Filter.Price.Minimum && ticket.Price <= Filter.Price.Maximum);
            }
            if (Filter?.TimeCreated != null)
            {
                query = query.Where(ticket => ticket.TimeCreated >= Filter.TimeCreated.Minimum && ticket.TimeCreated <= Filter.TimeCreated.Maximum);
            }
            if (Filter?.IsCanceled != null)
            {
                query = query.Where(ticket => ticket.IsCanceled == Filter.IsCanceled);
            }
            if (Filter?.IsPayed != null)
            {
                query = query.Where(ticket => ticket.IsPayed == Filter.IsPayed);
            }
            if (!string.IsNullOrEmpty(Filter?.UserFirstName))
            {
                query = query.Where(ticket => ticket.User.FirstName.ToLower().Contains(Filter.UserFirstName.ToLower()));
            }
            if (!string.IsNullOrEmpty(Filter?.UserLastName))
            {
                query = query.Where(ticket => ticket.User.LastName.ToLower().Contains(Filter.UserLastName.ToLower()));
            }
            if (!string.IsNullOrEmpty(Filter?.FromStopName))
            {
                query = query.Where(ticket => ticket.From.Name.ToLower().Contains(Filter.FromStopName.ToLower()));
            }
            if (!string.IsNullOrEmpty(Filter?.ToStopName))
            {
                query = query.Where(ticket => ticket.To.Name.ToLower().Contains(Filter.ToStopName.ToLower()));
            }
            if (Filter?.VehicleTypes != null)
            {
                query = query.Where(ticket => Filter.VehicleTypes.Contains(ticket.Seat.Link.Vehicle.VehicleType));
            }

            return query.ProjectTo<TicketDTO>();
        }
    }
}
