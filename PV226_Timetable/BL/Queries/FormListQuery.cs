﻿using System.Linq;
using AutoMapper.QueryableExtensions;
using BL.AppInfrastructure;
using BL.DTOs.Filters;
using BL.DTOs.Forms;
using DAL.Entities;
using Riganti.Utils.Infrastructure.Core;

namespace BL.Queries
{
    public class FormListQuery : AppQuery<FormDTO>
    {
        public FormFilter Filter { get; set; }

        public FormListQuery(IUnitOfWorkProvider provider) : base(provider) { }

        protected override IQueryable<FormDTO> GetQueryable()
        {
            IQueryable<Form> query = Context.Forms;

            if (Filter?.UserId > 0)
            {
                query = query.Where(form => form.Ticket.User.ID == Filter.UserId);
            }
            if (Filter?.Rating != null)
            {
                query = query.Where(form => form.Rating >= Filter.Rating.Minimum && form.Rating <= Filter.Rating.Maximum);
            }
            if (!string.IsNullOrEmpty(Filter?.UserFirstName))
            {
                query = query.Where(form => form.Ticket.User.FirstName.ToLower().Contains(Filter.UserFirstName.ToLower()));
            }
            if (!string.IsNullOrEmpty(Filter?.UserLastName))
            {
                query = query.Where(form => form.Ticket.User.LastName.ToLower().Contains(Filter.UserLastName.ToLower()));
            }

            return query.ProjectTo<FormDTO>();
        }
    }
}
