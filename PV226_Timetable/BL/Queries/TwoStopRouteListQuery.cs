﻿using AutoMapper.QueryableExtensions;
using BL.AppInfrastructure;
using BL.DTOs.Filters;
using BL.DTOs.TwoStopRoutes;
using DAL.Entities;
using Riganti.Utils.Infrastructure.Core;
using System.Linq;

namespace BL.Queries
{
    public class TwoStopRouteListQuery : AppQuery<TwoStopRouteDTO>
    {
        public TwoStopRouteFilter Filter { get; set; }

        public TwoStopRouteListQuery(IUnitOfWorkProvider provider) : base(provider) { }

        protected override IQueryable<TwoStopRouteDTO> GetQueryable()
        {
            IQueryable<TwoStopRoute> query = Context.TwoStopRoutes;

            if (Filter?.RouteId > 0)
            {
                query = query.Where(twoStopRoute => twoStopRoute.Route.ID == Filter.RouteId);
            }

            return query.ProjectTo<TwoStopRouteDTO>();
        }
    }
}
