﻿using System.Linq;

using BL.AppInfrastructure;
using BL.DTOs.Companies;
using BL.DTOs.Filters;
using DAL.Entities;

using AutoMapper.QueryableExtensions;

using Riganti.Utils.Infrastructure.Core;

namespace BL.Queries
{
    public class CompanyListQuery : AppQuery<CompanyListDTO>
    {
        public CompanyFilter Filter { get; set; }

        public CompanyListQuery(IUnitOfWorkProvider provider) : base(provider) { }

        protected override IQueryable<CompanyListDTO> GetQueryable()
        {
            IQueryable<Company> query = Context.Companies;

            if (!string.IsNullOrEmpty(Filter?.Name))
            {
                query = query.Where(category => category.Name.ToLower().Contains(Filter.Name.ToLower()));
            }

            return query.ProjectTo<CompanyListDTO>();
        }
    }
}
