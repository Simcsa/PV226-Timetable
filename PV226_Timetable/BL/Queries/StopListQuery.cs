﻿using System.Linq;
using AutoMapper.QueryableExtensions;
using BL.AppInfrastructure;
using BL.DTOs.Filters;
using BL.DTOs.Stops;
using DAL.Entities;
using Riganti.Utils.Infrastructure.Core;

namespace BL.Queries
{
    public class StopListQuery : AppQuery<StopDTO>
    {
        public StopFilter Filter { get; set; }

        public StopListQuery(IUnitOfWorkProvider provider) : base(provider) { }

        protected override IQueryable<StopDTO> GetQueryable()
        {
            IQueryable<Stop> query = Context.Stops;

            if (!string.IsNullOrEmpty(Filter?.Name))
            {
                query = query.Where(stop => stop.Name.ToLower().Contains(Filter.Name.ToLower()));
            }
            if (!string.IsNullOrEmpty(Filter?.Town))
            {
                query = query.Where(stop => stop.Town.ToLower().Contains(Filter.Town.ToLower()));
            }

            return query.ProjectTo<StopDTO>();
        }
    }
}
