﻿using DAL.Entities;
using Riganti.Utils.Infrastructure.Core;
using Riganti.Utils.Infrastructure.EntityFramework;

namespace BL.Repositories
{
    public class PhotoRepository : EntityFrameworkRepository<Photo, int>
    {
        public PhotoRepository(IUnitOfWorkProvider provider) : base(provider)
        {
        }
    }
}