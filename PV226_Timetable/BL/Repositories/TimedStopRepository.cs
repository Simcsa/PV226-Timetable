﻿using DAL.Entities;
using Riganti.Utils.Infrastructure.Core;
using Riganti.Utils.Infrastructure.EntityFramework;

namespace BL.Repositories
{
    public class TimedStopRepository : EntityFrameworkRepository<TimedStop, int>
    {
        public TimedStopRepository(IUnitOfWorkProvider provider) : base(provider)
        {
        }
    }
}