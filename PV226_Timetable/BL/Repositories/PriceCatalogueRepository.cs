﻿using DAL.Entities;
using Riganti.Utils.Infrastructure.Core;
using Riganti.Utils.Infrastructure.EntityFramework;

namespace BL.Repositories
{
    public class PriceCatalogueRepository : EntityFrameworkRepository<PriceCatalogue, int>
    {
        public PriceCatalogueRepository(IUnitOfWorkProvider provider) : base(provider)
        {
        }
    }
}