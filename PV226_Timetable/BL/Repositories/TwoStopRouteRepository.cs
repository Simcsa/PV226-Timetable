﻿using DAL.Entities;
using Riganti.Utils.Infrastructure.Core;
using Riganti.Utils.Infrastructure.EntityFramework;

namespace BL.Repositories
{
    public class TwoStopRouteRepository : EntityFrameworkRepository<TwoStopRoute, int>
    {
        public TwoStopRouteRepository(IUnitOfWorkProvider provider) : base(provider)
        {
        }
    }
}