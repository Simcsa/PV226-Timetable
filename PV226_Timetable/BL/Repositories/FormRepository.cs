﻿using DAL.Entities;
using Riganti.Utils.Infrastructure.Core;
using Riganti.Utils.Infrastructure.EntityFramework;

namespace BL.Repositories
{
    public class FormRepository : EntityFrameworkRepository<Form, int>
    {
        public FormRepository(IUnitOfWorkProvider provider) : base(provider)
        {
        }
    }
}