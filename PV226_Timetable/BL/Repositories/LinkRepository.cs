﻿using DAL.Entities;
using Riganti.Utils.Infrastructure.Core;
using Riganti.Utils.Infrastructure.EntityFramework;

namespace BL.Repositories
{
    public class LinkRepository : EntityFrameworkRepository<Link, int>
    {
        public LinkRepository(IUnitOfWorkProvider provider) : base(provider)
        {
        }
    }
}