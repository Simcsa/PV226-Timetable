﻿using System;
using System.Data.Entity;
using BL.Services;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using DAL;
using Riganti.Utils.Infrastructure.Core;
using Riganti.Utils.Infrastructure.EntityFramework;

namespace BL.AppInfrastructure
{
    public class BusinessLayerInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(

                Component.For<IUnitOfWorkProvider>()
                    .ImplementedBy<AppUnitOfWorkProvider>()
                    .LifestyleSingleton(),

                Component.For<IUnitOfWorkRegistry>()
                    .Instance(new HttpContextUnitOfWorkRegistry(new ThreadLocalUnitOfWorkRegistry()))
                    .LifestyleSingleton(),

                Component.For<Func<DbContext>>()
                    .Instance(() => new TimetableDbContext())
                    .LifestyleTransient(),

                Component.For(typeof(IRepository<,>))
                    .ImplementedBy(typeof(EntityFrameworkRepository<,>))
                    .LifestyleTransient(),

                Classes.FromAssemblyContaining<AppUnitOfWork>()
                    .BasedOn(typeof(AppQuery<>))
                    .LifestyleTransient(),

                Classes.FromAssemblyContaining<AppUnitOfWork>()
                    .BasedOn(typeof(IRepository<,>))
                    .LifestyleTransient(),

                Classes.FromThisAssembly()
                        .BasedOn(typeof(CrudServiceBase<,,,,>))
                        .WithServiceDefaultInterfaces()
                        .LifestyleTransient(),

                Classes.FromThisAssembly()
                        .BasedOn(typeof(ListCrudServiceBase<,,,,,,,>))
                        .WithServiceDefaultInterfaces()
                        .LifestyleTransient(),

                Classes.FromThisAssembly()
                    .InNamespace("BL.Facades")
                    .LifestyleTransient()

            );
        }
    }
}
