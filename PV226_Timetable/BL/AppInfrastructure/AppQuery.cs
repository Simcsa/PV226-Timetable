﻿using DAL;

using Riganti.Utils.Infrastructure.Core;
using Riganti.Utils.Infrastructure.EntityFramework;

namespace BL.AppInfrastructure
{
    public abstract class AppQuery<T> : EntityFrameworkQuery<T>
    {
        public new TimetableDbContext Context => (TimetableDbContext)base.Context;

        protected AppQuery(IUnitOfWorkProvider provider) : base(provider) { }
    }
}
