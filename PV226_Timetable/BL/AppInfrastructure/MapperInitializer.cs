﻿using BL.DTOs.Companies;
using DAL.Entities;

using AutoMapper;
using BL.DTOs.Discounts;
using BL.DTOs.Forms;
using BL.DTOs.Links;
using BL.DTOs.PriceCatalogues;
using BL.DTOs.Routes;
using BL.DTOs.Seats;
using BL.DTOs.Stops;
using BL.DTOs.Tickets;
using BL.DTOs.TimedStops;
using BL.DTOs.TwoStopRoutes;
using BL.DTOs.Users;
using BL.DTOs.Vehicles;
using System;
using System.Collections.Generic;

namespace BL.AppInfrastructure
{
    public class MapperInitializer
    {
        public static void ConfigureMapping()
        {
            Mapper.Initialize(config =>
            {
                //Companies
                config.CreateMap<Company, CompanyDetailDTO>()
                    .ForMember(dest => dest.PricePerKm, opts => opts.MapFrom(src => src.PriceCatalogue.PricePerKm))
                    .ForMember(dest => dest.PriceCatalogueId, opts => opts.MapFrom(src => src.PriceCatalogue.ID));

                config.CreateMap<CompanyDetailDTO, Company>()
                    .ForMember(dest => dest.PriceCatalogue, opts => opts.MapFrom(src => src));
                config.CreateMap<CompanyDetailDTO, PriceCatalogue>()
                    .ForMember(dest => dest.PricePerKm, opts => opts.MapFrom(src => src.PricePerKm))
                    .ForMember(dest => dest.ID, opts => opts.MapFrom(src => src.PriceCatalogueId));

                config.CreateMap<Company, CompanyListDTO>()
                    .ForMember(dest => dest.PricePerKm, opts => opts.MapFrom(src => src.PriceCatalogue.PricePerKm));

                // Discounts
                config.CreateMap<Discount, DiscountDetailDTO>().ReverseMap();
                config.CreateMap<Discount, DiscountCreateDTO>()
                        .ForMember(dest => dest.PriceCatalogueId, opts => opts.MapFrom(src => src.ID)).ReverseMap();

                // Forms
                config.CreateMap<Form, FormDTO>()
                    .ForMember(dest => dest.UserFirstName, opts => opts.MapFrom(src => src.Ticket.User.FirstName))
                    .ForMember(dest => dest.UserLastName, opts => opts.MapFrom(src => src.Ticket.User.LastName)).ReverseMap();

                config.CreateMap<Form, FormDTOCreate>()
                    .ForMember(dest => dest.TicketID, opts => opts.MapFrom(src => src.Ticket.ID)).ReverseMap();

                config.CreateMap<FormDTOCreate, Form>()
                    .ForMember(dest => dest.Ticket, opts => opts.MapFrom(src => src));
                config.CreateMap<FormDTOCreate, Ticket>()
                   .ForMember(dest => dest.ID, opts => opts.MapFrom(src => src.TicketID));

                // Cannot be resolved directly to the child type dest.Ticket.User.FirstName
                //config.CreateMap<FormDTO, Form>()
                //     .ForMember(dest => dest.Ticket, opts => opts.MapFrom(src => src));
                // config.CreateMap<FormDTO, Ticket>()
                //    .ForMember(dest => dest.Seat, opts => opts.MapFrom(src => src));
                //  config.CreateMap<FormDTO, User>()
                //   .ForMember(dest => dest.FirstName, opts => opts.MapFrom(src => src.UserFirstName))
                //    .ForMember(dest => dest.LastName, opts => opts.MapFrom(src => src.UserLastName));

                // Links
                config.CreateMap<Link, LinkDTO>()
                    .ForMember(dest => dest.Route, opts => opts.MapFrom(src => src.Route))
                    .ForMember(dest => dest.VehicleType, opts => opts.MapFrom(src => src.Vehicle.VehicleType))
                    .ForMember(dest => dest.VehicleID, opts => opts.MapFrom(src => src.Vehicle.ID));

                config.CreateMap<LinkDTO, Link>()
                    .ForMember(dest => dest.Vehicle, opts => opts.MapFrom(src => src));
                config.CreateMap<LinkDTO, Route>()
                    .ForMember(dest => dest.ID, opts => opts.MapFrom(src => src.RouteID));
                config.CreateMap<LinkDTO, Vehicle>()
                    .ForMember(dest => dest.ID, opts => opts.MapFrom(src => src.VehicleID))
                    .ForMember(dest => dest.VehicleType, opts => opts.MapFrom(src => src.VehicleType));

                // Price catalogues
                config.CreateMap<PriceCatalogue, PriceCatalogueDTO>().ReverseMap();

                // Routes
                config.CreateMap<Route, RouteDTO>()
                    .ForMember(dest => dest.TwoStopRoutes, opts => opts.ResolveUsing<TwoStopRouteResolver>());

                config.CreateMap<RouteDTO, Route>()
                    .ForMember(dest => dest.TwoStopRoutes, opts => opts.ResolveUsing<TwoStopRouteResolverReverse>());

                // Seats
                config.CreateMap<Seat, SeatDTO>()
                    .ForMember(dest => dest.LinkID, opts => opts.MapFrom(src => src.Link.ID));

                config.CreateMap<SeatDTO, Seat>()
                    .ForMember(dest => dest.Link, opts => opts.MapFrom(src => src));
                config.CreateMap<SeatDTO, Link>()
                   .ForMember(dest => dest.ID, opts => opts.MapFrom(src => src.LinkID));

                // Stops
                config.CreateMap<Stop, StopDTO>().ReverseMap();
                config.CreateMap<Stop, StopDTOWihPhotos>().ReverseMap();

                // Tickets
                config.CreateMap<Ticket, TicketDTO>()
                    .ForMember(dest => dest.FormID, opts => opts.MapFrom(src => src.Form.ID))
                    .ForMember(dest => dest.FromStopID, opts => opts.MapFrom(src => src.From.ID))
                    .ForMember(dest => dest.FromStopName, opts => opts.MapFrom(src => src.From.Name))
                    .ForMember(dest => dest.ToStopID, opts => opts.MapFrom(src => src.To.ID))
                    .ForMember(dest => dest.ToStopName, opts => opts.MapFrom(src => src.To.Name))
                    .ForMember(dest => dest.UserId, opts => opts.MapFrom(src => src.User.ID))
                    .ForMember(dest => dest.SeatID, opts => opts.MapFrom(src => src.Seat.ID))
                    .ForMember(dest => dest.SeatNumber, opts => opts.MapFrom(src => src.Seat.Number)).ReverseMap();

                //config.CreateMap<TicketDTO, Ticket>()
                //     .ForMember(dest => dest.Form, opts => opts.MapFrom(src => src))
                //     .ForMember(dest => dest.User, opts => opts.MapFrom(src => src))
                //     .ForMember(dest => dest.Seat, opts => opts.MapFrom(src => src))
                //     .ForMember(dest => dest.From, opts => opts.MapFrom(src => new Ticket() { ID = src.FromStopID }))
                //     .ForMember(dest => dest.To, opts => opts.MapFrom(src => new Ticket() { ID = src.ToStopID } ));
                //config.CreateMap<TicketDTO, Form>()
                //    .ForMember(dest => dest.ID, opts => opts.MapFrom(src => src.FormID));
                //config.CreateMap<TicketDTO, User>()
                //    .ForMember(dest => dest.ID, opts => opts.MapFrom(src => src.UserId));
                //config.CreateMap<TicketDTO, Seat>()
                //    .ForMember(dest => dest.ID, opts => opts.MapFrom(src => src.SeatID))
                //   .ForMember(dest => dest.Number, opts => opts.MapFrom(src => src.SeatNumber));

                config.CreateMap<Ticket, ReturnTicketDTO>()
                    .ForMember(dest => dest.CompanyID, opts => opts.MapFrom(src => src.Seat.Link.Vehicle.Company.ID))
                    .ReverseMap();

                config.CreateMap<Ticket, TicketDTOCreate>()
                    .ForMember(dest => dest.FromStopID, opts => opts.MapFrom(src => src.From.ID))
                    .ForMember(dest => dest.ToStopID, opts => opts.MapFrom(src => src.To.ID))
                    .ForMember(dest => dest.UserId, opts => opts.MapFrom(src => src.User.ID))
                    .ReverseMap();

                // Timed stops
                config.CreateMap<TimedStop, TimedStopDTO>()
                    .ForMember(dest => dest.StopId, opts => opts.MapFrom(src => src.Stop.ID));

                config.CreateMap<TimedStopDTO, TimedStop>()
                    .ForMember(dest => dest.Stop, opts => opts.MapFrom(src => src));
                config.CreateMap<TimedStopDTO, Stop>()
                    .ForMember(dest => dest.ID, opts => opts.MapFrom(src => src.StopId));

                // TwoStopRoutes
                config.CreateMap<TwoStopRoute, TwoStopRouteDTO>()
                    .ForMember(dest => dest.ArriveStopId, opts => opts.MapFrom(src => src.ArriveStop == null ? 0 : src.ArriveStop.ID))
                    .ForMember(dest => dest.LeaveStopId, opts => opts.MapFrom(src => src.LeaveStop == null ? 0 : src.LeaveStop.ID))
                    .ForMember(dest => dest.RouteId, opts => opts.MapFrom(src => src.Route == null ? 0 : src.Route.ID))
                    .ReverseMap();

                // Users
                config.CreateMap<User, UserDTO>().ReverseMap();

                //Vehicles
                config.CreateMap<Vehicle, VehicleDTO>()
                    .ForMember(dest => dest.CompanyName, opts => opts.MapFrom(src => src.Company.Name));

                config.CreateMap<Vehicle, VehicleDTOCreate>()
                    .ForMember(dest => dest.CompanyId, opts => opts.MapFrom(src => src.Company.ID));

                config.CreateMap<VehicleDTO, Vehicle>()
                    .ForMember(dest => dest.Company, opts => opts.MapFrom(src => src));
                config.CreateMap<VehicleDTO, Company>()
                    .ForMember(dest => dest.Name, opts => opts.MapFrom(src => src.CompanyName));

                config.CreateMap<VehicleDTOCreate, Vehicle>()
                  .ForMember(dest => dest.Company, opts => opts.MapFrom(src => src));
                config.CreateMap<VehicleDTOCreate, Company>()
                    .ForMember(dest => dest.ID, opts => opts.MapFrom(src => src.CompanyId));
            });
        }
    }

    public class TwoStopRouteResolver : IValueResolver<Route, RouteDTO, List<TwoStopRoute>>
    {
        public List<TwoStopRoute> Resolve(Route source, RouteDTO destination, List<TwoStopRoute> destMember, ResolutionContext context)
        {
            List<TwoStopRoute> results = new List<TwoStopRoute>();

            if(source.TwoStopRoutes != null)
            {
                for (int i = 1; i <= source.TwoStopRoutes.Count; i++)
                {
                    TwoStopRoute twoStopRoute = source.TwoStopRoutes.Find(tsr => tsr.Order == i);
                    results.Add(twoStopRoute);
                }
            }

            return results;
        }
    }

    public class TwoStopRouteResolverReverse : IValueResolver<RouteDTO, Route, List<TwoStopRoute>>
    {
        public List<TwoStopRoute> Resolve(RouteDTO source, Route destination, List<TwoStopRoute> destMember, ResolutionContext context)
        {
            return source.TwoStopRoutes;
        }
    }
}
