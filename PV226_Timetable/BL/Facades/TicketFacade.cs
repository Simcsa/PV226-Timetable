﻿using System.Collections.Generic;
using BL.DTOs.Filters;
using BL.DTOs.Tickets;
using BL.Services.Tickets;

namespace BL.Facades
{
    public class TicketFacade
    {
        #region Dependencies

        private readonly ITicketService ticketService;

        public TicketFacade(ITicketService ticketService)
        {
            this.ticketService = ticketService;
        }

        #endregion

        #region TicketManagement
        
        /// <summary>
        /// Cancels ticket if possible.
        /// </summary>
        /// <param name="returnTicketDTO">ticket to be cancelled</param>
        /// <returns>true if ticket was cancelled, false otherwise</returns>
        public bool ReturnTicket(ReturnTicketDTO returnTicketDTO)
        {
            return ticketService.ReturnTicket(returnTicketDTO);
        }

        /// <summary>
        /// If ticket already exists it marks it as payed, else it creates new ticket.
        /// </summary>
        /// <param name="ticketDto">ticket to be bought</param>
        /// <returns>true if ticket was bought, false otherwise</returns>
        public bool BuyTicket(TicketDTOCreate ticketDto)
        {
            return ticketService.BuyTicket(ticketDto);
        }

        /// <summary>
        /// Returns ticket with given id.
        /// </summary>
        /// <param name="id">id of the ticket</param>
        /// <returns></returns>
        public TicketDTO GetTicket(int id)
        {
            return ticketService.Get(id);
        }

        /// <summary>
        /// Creates new ticket.
        /// </summary>
        /// <param name="ticketDTO">DTO ticket object</param>
        public void CreateTicket(TicketDTOCreate ticketDTO)
        {
            ticketService.Create(ticketDTO);
        }


        /// <summary>
        /// Updates ticket according to his/her ID
        /// </summary>
        /// <param name="ticketDTO">ticket details</param>
        public void UpdateTicket(TicketDTOUpdate ticketDTO)
        {
            ticketService.Update(ticketDTO);
        }

        /// <summary>
        /// Deletes ticket with given id.
        /// </summary>
        /// <param name="id">id of ticket to be deleted</param>
        public void DeleteTicket(int id)
        {
            ticketService.Delete(id);
        }

        public IEnumerable<TicketDTO> List(TicketFilter filter)
        {
            return ticketService.List(filter);
        }

        #endregion

    }
}
