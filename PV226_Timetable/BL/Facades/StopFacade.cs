﻿using BL.DTOs.Filters;
using BL.DTOs.Stops;
using BL.Services.Stops;
using DAL.Entities;
using System.Collections.Generic;

namespace BL.Facades
{
    public class StopFacade
    {
        #region Dependencies

        private readonly IStopService stopService;

        public StopFacade(IStopService stopService)
        {
            this.stopService = stopService;
        }

        #endregion

        #region LinkManagement


        /// <summary>
        /// Returns stop with given id.
        /// </summary>
        /// <param name="id">id of the stop</param>
        public StopDTO Get(int id)
        {
            return stopService.Get(id);
        }

        public IEnumerable<StopDTO> List(StopFilter filter)
        {
            return stopService.List(filter);
        }

        public Photo GetPhoto(int id)
        {
            return stopService.GetPhoto(id); 
        }

        public List<int> GetPhotoIds(int stopId)
        {
            return stopService.GetPhotoIds(stopId);
        }

        public void AddPhotos(StopDTOWihPhotos stopDto)
        {
            stopService.AddPhotos(stopDto);
        }

        #endregion
    }
}
