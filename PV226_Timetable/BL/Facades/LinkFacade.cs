﻿using System;
using System.Collections.Generic;
using BL.DTOs.Filters;
using BL.DTOs.Links;
using BL.Services.Links;

namespace BL.Facades
{
    public class LinkFacade
    {
        #region Dependencies

        private readonly ILinkService linkService;

        public LinkFacade(ILinkService linkService)
        {
            this.linkService = linkService;
        }

        #endregion

        #region LinkManagement
        

        /// <summary>
        /// Returns link with given id.
        /// </summary>
        /// <param name="id">id of the link</param>
        /// <returns></returns>
        public LinkDTO GetLink(int id)
        {
            return linkService.Get(id);
        }

        /// <summary>
        /// Creates new link.
        /// </summary>
        /// <param name="linkDTO">DTO link object</param>
        public void CreateLink(LinkDTO linkDTO)
        {
            int id = linkService.Create(linkDTO);
            linkDTO.ID = id;
        }


        /// <summary>
        /// Updates link according to his/her ID
        /// </summary>
        /// <param name="linkDTO">link details</param>
        public void UpdateLink(LinkDTO linkDTO)
        {
            linkService.Update(linkDTO);
        }

        /// <summary>
        /// Deletes link with given id.
        /// </summary>
        /// <param name="id">id of link to be deleted</param>
        public void DeleteLink(int id)
        {
            linkService.Delete(id);
        }


        public IEnumerable<LinkDTO> List(LinkFilter filter)
        {
            return linkService.List(filter);
        }

        #endregion

    }
}
