﻿using BL.DTOs.Forms;
using BL.DTOs.Tickets;
using BL.DTOs.Users;
using BL.Services.Seats;
using BL.Services.Tickets;
using BL.Services.Users;

namespace BL.Facades
{
    public class UserFacade
    {
        #region Dependencies

        private readonly IUserService userService;
        private readonly ITicketService ticketService;
        private readonly ISeatService seatService;

        public UserFacade(IUserService userService, ITicketService ticketService, ISeatService seatService)
        {
            this.userService = userService;
            this.ticketService = ticketService;
            this.seatService = seatService;
        }

        #endregion

        #region UserManagement

        public void SendMail(TicketDTO ticketDTO)
        {
            ticketService.SendEmail(ticketDTO);
        }

        /// <summary>
        /// Returns user with given id.
        /// </summary>
        /// <param name="id">id of the user</param>
        /// <returns></returns>
        public UserDTO GetUser(int id)
        {
            return userService.Get(id);
        }
        
        /// <summary>
        /// Creates new user.
        /// </summary>
        /// <param name="userDTO">DTO user object</param>
        public void CreateUser(UserDTO userDTO)
        {
            userService.Create(userDTO);
        }


        /// <summary>
        /// Updates user according to his/her ID
        /// </summary>
        /// <param name="userDTO">user details</param>
        public void UpdateUser(UserDTO userDTO)
        {
            userService.Update(userDTO);
        }

        /// <summary>
        /// Deletes user with given id.
        /// </summary>
        /// <param name="id">id of user to be deleted</param>
        public void DeleteUser(int id)
        {
            userService.Delete(id);
        }
        
        #endregion

    }
}