﻿using System.Collections.Generic;
using BL.DTOs.Companies;
using BL.DTOs.Filters;
using BL.Services.Companies;
using BL.Services.PriceCatalogues;
using BL.DTOs.PriceCatalogues;
using BL.DTOs.Discounts;

namespace BL.Facades
{
    public class CompanyFacade
    {
        #region Dependencies

        private readonly ICompanyService companyService;
        private readonly IPriceCatalogueService catalogueService;


        public CompanyFacade(ICompanyService companyService, IPriceCatalogueService catalogueService)
        {
            this.companyService = companyService;
            this.catalogueService = catalogueService;
        }

        #endregion
        
        #region CompanyManagement

        /// <summary>
        /// Returns company with given id.
        /// </summary>
        /// <param name="id">id of the company</param>
        /// <returns></returns>
        public CompanyDetailDTO GetCompany(int id)
        {
            return companyService.Get(id);
        }

        /// <summary>
        /// Creates new company.
        /// </summary>
        /// <param name="companyDTO">DTO company details object</param>
        public void CreateCompany(CompanyDetailDTO companyDTO)
        {
            int id = companyService.Create(companyDTO);
            companyDTO.ID = id;
        }

        /// <summary>
        /// Updates company according to his/her ID
        /// </summary>
        /// <param name="companyDTO">company details</param>
        public void UpdateCompany(CompanyDetailDTO companyDTO)
        {
            companyService.Update(companyDTO);
        }

        /// <summary>
        /// Deletes company with given id.
        /// </summary>
        /// <param name="id">id of company to be deleted</param>
        public void DeleteCompany(int id)
        {
            companyService.Delete(id);
        }

        /// <summary>
        /// Gets the price catalogue by id.
        /// </summary>
        /// <param name="priceCatalogueId">The price catalogue identifier.</param>
        public PriceCatalogueDTO GetPriceCatalogue(int priceCatalogueId)
        {
            return catalogueService.Get(priceCatalogueId);
        }

        public bool AddPriceCatalogueDiscount(DiscountCreateDTO discountDTO)
        {
            return catalogueService.AddDiscount(discountDTO);
        }

        public bool EditPriceCatalogueDiscount(DiscountCreateDTO discountDTO)
        {
            return catalogueService.UpdateDiscount(discountDTO);
        }

        public bool RemovePriceCatalogueDiscount(DiscountCreateDTO discountDTO)
        {
            return catalogueService.RemoveDiscount(discountDTO);
        }

        public IEnumerable<CompanyListDTO> List(CompanyFilter filter)
        {
            return companyService.List(filter);
        }

        #endregion

    }
}
