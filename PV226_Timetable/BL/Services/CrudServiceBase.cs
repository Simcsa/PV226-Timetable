﻿using AutoMapper;
using BL.DTOs.Common;
using Riganti.Utils.Infrastructure.Core;

namespace BL.Services
{
    public abstract class CrudServiceBase<TEntity, TKey, TCreateDTO, TUpdateDTO, TDetailDTO> : ICrudServiceBase<TEntity, TKey, TCreateDTO, TUpdateDTO, TDetailDTO> 
        where TEntity : IEntity<TKey>
        where TDetailDTO : DTOBase<TKey>
        where TUpdateDTO : DTOBase<TKey>
    {
        public IUnitOfWorkProvider UnitOfWorkProvider { get; set; }

        protected abstract IRepository<TEntity, TKey> GetRepository();

        public virtual TKey Create(TCreateDTO dto)
        {
            using (var uow = UnitOfWorkProvider.Create())
            {
                var entity = Mapper.Map<TEntity>(dto);
                GetRepository().Insert(entity);

                uow.Commit();
                return entity.ID;
            }
        }

        public virtual void Update(TUpdateDTO dto)
        {
            using (var uow = UnitOfWorkProvider.Create())
            {
                var entity = GetRepository().GetById(dto.ID);
                Mapper.Map(dto, entity);
                GetRepository().Update(entity);

                uow.Commit();
            }
        }

        public void Delete(TKey id)
        {
            using (var uow = UnitOfWorkProvider.Create())
            {
                GetRepository().Delete(id);

                uow.Commit();
            }
        }

        public virtual TDetailDTO Get(TKey id)
        {
            using (UnitOfWorkProvider.Create())
            {
                var entity = GetRepository().GetById(id);
                return Mapper.Map<TDetailDTO>(entity);
            }
        }
    }
}
