﻿using System.Collections.Generic;

namespace BL.Services
{
    public interface IListCrudServiceBase<TEntity, TKey, TCreateDTO, TUpdateDTO, TDetailDTO, TListDTO, TPagedListDTO, TQueryFilter> : ICrudServiceBase<TEntity, TKey, TCreateDTO, TUpdateDTO, TDetailDTO>
    {
        /// <summary>
        /// Lists entities by <paramref name="filter"/>.
        /// </summary>
        /// <param name="filter">filter to query by</param>
        /// <returns>list of entities</returns>
        IEnumerable<TListDTO> List(TQueryFilter filter);

        /// <summary>
        /// Lists entities by <paramref name="filter"/> according to requested page <paramref name="requiredPage"/>.
        /// </summary>
        /// <param name="filter">filter to query by</param>
        /// <param name="requiredPage">required page id</param>
        /// <returns>list of entities for required page</returns>
        TPagedListDTO List(TQueryFilter filter, int requiredPage);
    }
}
