﻿using DAL.Entities;
using BL.DTOs.Filters;
using BL.DTOs.Seats;
using System.Collections.Generic;

namespace BL.Services.Seats
{
    public interface ISeatService : IListCrudServiceBase<Seat, int, SeatDTO, SeatDTO, SeatDTO, SeatDTO, SeatListQueryResultDTO, SeatFilter>
    {
        /// <summary>
        /// Finds the empty seats for given link.
        /// </summary>
        /// <param name="linkId">link id</param>
        /// <returns>list of empty seats</returns>
        IEnumerable<SeatDTO> FindEmptySeats(int linkId);
    }
}
