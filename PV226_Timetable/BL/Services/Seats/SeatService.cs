﻿using DAL.Entities;
using BL.DTOs.Filters;
using BL.DTOs.Seats;
using BL.Queries;
using BL.Repositories;
using Riganti.Utils.Infrastructure.Core;
using System;
using System.Collections.Generic;
using AutoMapper;

namespace BL.Services.Seats
{
    public class SeatService : ListCrudServiceBase<Seat, int, SeatDTO, SeatDTO, SeatDTO, SeatDTO, SeatListQueryResultDTO, SeatFilter>, ISeatService
    {
        private IRepository<Seat, int> Repository { get; set; }

        private readonly LinkRepository linkRepository;

        private SeatListQuery Query { get; set; }

        protected override IRepository<Seat, int> GetRepository()
        {
            return Repository;
        }

        protected override IQuery<SeatDTO> GetQuery(SeatFilter filter)
        {
            Query.Filter = filter;
            return Query;
        }

        protected override int GetPageSize()
        {
            return 10;
        }

        public SeatService(SeatRepository seatRepository, SeatListQuery seatListQuery, LinkRepository linkRepository)
        {
            Repository = seatRepository;
            Query = seatListQuery;
            this.linkRepository = linkRepository;
        }

        public IEnumerable<SeatDTO> FindEmptySeats(int linkId)
        {
            return List(new SeatFilter() { IsOccupied = false, LinkId = linkId });
        }

        public override int Create(SeatDTO seatDto)
        {
            using (var uow = UnitOfWorkProvider.Create())
            {
                Link link = linkRepository.GetById(seatDto.LinkID);

                Seat seat = Mapper.Map<Seat>(seatDto);
                seat.Link = link;

                GetRepository().Insert(seat);

                uow.Commit();

                return seat.ID;
            }
        }
    }
}
