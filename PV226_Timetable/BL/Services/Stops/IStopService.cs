﻿using System.Collections.Generic;
using System.Drawing;
using BL.DTOs.Filters;
using BL.DTOs.Stops;
using DAL.Entities;

namespace BL.Services.Stops
{
    public interface IStopService: IListCrudServiceBase<Stop, int, StopDTO, StopDTO, StopDTO, StopDTO, StopListQueryResultDTO, StopFilter>
    {
        /// <summary>
        /// Adds photos to stop.
        /// </summary>
        /// <param name="stopdto">stop to add images to</param>
        void AddPhotos(StopDTOWihPhotos stopDto);

        /// <summary>
        /// Retrieves all photos ids from the stop.
        /// </summary>
        /// <param name="stopId">stop to get photos from</param>
        /// <returns>list of ids of photos</returns>
        List<int> GetPhotoIds(int stopId);

        Photo GetPhoto(int id);
    }
}
