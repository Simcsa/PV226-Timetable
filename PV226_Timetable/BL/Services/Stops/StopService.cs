﻿using System;
using System.Collections.Generic;
using BL.DTOs.Filters;
using BL.DTOs.Stops;
using BL.Queries;
using BL.Repositories;
using DAL.Entities;
using Riganti.Utils.Infrastructure.Core;
using AutoMapper;

namespace BL.Services.Stops
{
    public class StopService : ListCrudServiceBase<Stop, int, StopDTO, StopDTO, StopDTO, StopDTO, StopListQueryResultDTO, StopFilter>, IStopService
    {
        private IRepository<Stop, int> Repository { get; set; }

        private readonly PhotoRepository photoRepository;

        private StopListQuery Query { get; set; }

        protected override IRepository<Stop, int> GetRepository()
        {
            return Repository;
        }

        protected override IQuery<StopDTO> GetQuery(StopFilter filter)
        {
            Query.Filter = filter;
            return Query;
        }

        protected override int GetPageSize()
        {
            return 10;
        }

        public StopService(StopRepository stopRepository, StopListQuery stopListQuery, PhotoRepository photoRepository)
        {
            Repository = stopRepository;
            Query = stopListQuery;
            this.photoRepository = photoRepository;
        }

        public void AddPhotos(StopDTOWihPhotos stopDto)
        {
            using (var uow = UnitOfWorkProvider.Create())
            {
                var stop = GetRepository().GetById(stopDto.ID, s => s.Photos);

                // add photos to database
                foreach (Photo p in stopDto.Photos)
                {
                    photoRepository.Insert(p);
                }

                // add photos to list of stop photos
                stop.Photos.AddRange(stopDto.Photos);
                
                GetRepository().Update(stop);

                uow.Commit();
            }
        }

        public List<int> GetPhotoIds(int stopId)
        {
            using (UnitOfWorkProvider.Create())
            {
                var stop = GetRepository().GetById(stopId, s => s.Photos);

                return stop.Photos.ConvertAll(new Converter<Photo, int>(photo => photo.ID));
            }
        }

        public Photo GetPhoto(int id)
        {
            using (UnitOfWorkProvider.Create())
            {
                return photoRepository.GetById(id);
            }
        }
    }
}
