﻿using BL.DTOs.Companies;
using BL.DTOs.Filters;
using DAL.Entities;

namespace BL.Services.Companies
{
    public interface ICompanyService : IListCrudServiceBase<Company, int, CompanyDetailDTO, CompanyDetailDTO, CompanyDetailDTO, CompanyListDTO, CompanyListQueryResultDTO, CompanyFilter>
    {
    }
}