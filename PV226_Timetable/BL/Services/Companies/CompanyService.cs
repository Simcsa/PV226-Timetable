﻿using AutoMapper;
using BL.DTOs.Companies;
using BL.DTOs.Filters;
using BL.Queries;
using BL.Repositories;
using DAL.Entities;

using Riganti.Utils.Infrastructure.Core;
using System.Collections.Generic;
using System;

namespace BL.Services.Companies
{
    public class CompanyService : ListCrudServiceBase<Company, int, CompanyDetailDTO, CompanyDetailDTO, CompanyDetailDTO, CompanyListDTO, CompanyListQueryResultDTO, CompanyFilter>, ICompanyService
    {
        private IRepository<Company, int> Repository { get; set; }

        private readonly PriceCatalogueRepository priceCatalogueRepository;
       
        private CompanyListQuery Query { get; set; }

        protected override IRepository<Company, int> GetRepository()
        {
            return Repository;
        }

        protected override IQuery<CompanyListDTO> GetQuery(CompanyFilter filter)
        {
            Query.Filter = filter;
            Query.AddSortCriteria("Name");
            return Query;
        }

        protected override int GetPageSize()
        {
            return 10;
        }

        public CompanyService(CompanyRepository companyRepository, CompanyListQuery companyListQuery, PriceCatalogueRepository priceCatalogueRepository)
        {
            Repository = companyRepository;
            Query = companyListQuery;
            this.priceCatalogueRepository = priceCatalogueRepository;
        }

        public override int Create(CompanyDetailDTO companyDetailDto)
        {
            using (var uow = UnitOfWorkProvider.Create())
            {
                PriceCatalogue catalogue = new PriceCatalogue()
                {
                    PricePerKm = companyDetailDto.PricePerKm,
                    Discounts = new HashSet<Discount>()
                };
                priceCatalogueRepository.Insert(catalogue);

                Company company = Mapper.Map<Company>(companyDetailDto);
                company.PriceCatalogue = catalogue;

                GetRepository().Insert(company);

                uow.Commit();

                return company.ID;
            }
        }

        public override void Update(CompanyDetailDTO companyDetailDto)
        {
            using (var uow = UnitOfWorkProvider.Create())
            {
                PriceCatalogue catalogue = priceCatalogueRepository.GetById(companyDetailDto.PriceCatalogueId.Value);

                Company company = Mapper.Map<Company>(companyDetailDto);
                company.PriceCatalogue = catalogue;

                GetRepository().Update(company);

                uow.Commit();
            }
        }

        public override CompanyDetailDTO Get(int id)
        {
            using (var uow = UnitOfWorkProvider.Create())
            {
                var company = Repository.GetById(id, c => c.PriceCatalogue);
                return Mapper.Map<CompanyDetailDTO>(company);
            }
        }
    }
}