﻿using AutoMapper;
using BL.DTOs.Filters;
using BL.DTOs.Vehicles;
using BL.Queries;
using BL.Repositories;
using DAL.Entities;
using Riganti.Utils.Infrastructure.Core;

namespace BL.Services.Vehicles
{
    public class VehicleService : ListCrudServiceBase<Vehicle, int, VehicleDTOCreate, VehicleDTOCreate, VehicleDTO, VehicleDTO, VehicleListQueryResultDTO, VehicleFilter>, IVehicleService
    {
        private IRepository<Vehicle, int> Repository { get; set; }

        private readonly CompanyRepository companyRepository;

        private VehicleListQuery Query { get; set; }

        protected override IRepository<Vehicle, int> GetRepository()
        {
            return Repository;
        }

        protected override IQuery<VehicleDTO> GetQuery(VehicleFilter filter)
        {
            Query.Filter = filter;
            return Query;
        }

        protected override int GetPageSize()
        {
            return 10;
        }

        public VehicleService(VehicleRepository vehicleRepository, VehicleListQuery vehicleListQuery, CompanyRepository companyRepository)
        {
            Repository = vehicleRepository;
            Query = vehicleListQuery;
            this.companyRepository = companyRepository;
        }

        public override int Create(VehicleDTOCreate dto)
        {
            using (var uow = UnitOfWorkProvider.Create())
            {
                Company company = companyRepository.GetById(dto.CompanyId);

                Vehicle vehicle = Mapper.Map<Vehicle>(dto);
                vehicle.Company = company;

                GetRepository().Insert(vehicle);

                uow.Commit();
                return vehicle.ID;
            }
        }
    }
}
