﻿using BL.DTOs.Filters;
using BL.DTOs.Vehicles;
using DAL.Entities;

namespace BL.Services.Vehicles
{
    public interface IVehicleService : IListCrudServiceBase<Vehicle, int, VehicleDTOCreate, VehicleDTOCreate, VehicleDTO, VehicleDTO, VehicleListQueryResultDTO, VehicleFilter>
    {
    }
}
