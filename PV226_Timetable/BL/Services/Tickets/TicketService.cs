﻿using BL.DTOs.Filters;
using BL.DTOs.Tickets;
using BL.Queries;
using BL.Repositories;
using DAL.Entities;
using Riganti.Utils.Infrastructure.Core;
using System.Net.Mail;
using System.Linq;
using System;
using AutoMapper;
using System.Collections.Generic;

namespace BL.Services.Tickets
{
    public class TicketService : ListCrudServiceBase<Ticket, int, TicketDTOCreate, TicketDTOUpdate, TicketDTO, TicketDTO, TicketListQueryResultDTO, TicketFilter>, ITicketService
    {
        private const string TIMETABLE_MAIL = "timetable@gmail.com";

        private readonly CompanyRepository companyRepository;
        private readonly UserRepository userRepository;
        private readonly SeatRepository seatRepository;
        private readonly StopRepository stopRepository;

        private IRepository<Ticket, int> Repository { get; set; }

        private TicketListQuery Query { get; set; }

        protected override IRepository<Ticket, int> GetRepository()
        {
            return Repository;
        }

        protected override IQuery<TicketDTO> GetQuery(TicketFilter filter)
        {
            Query.Filter = filter;
            return Query;
        }

        protected override int GetPageSize()
        {
            return 10;
        }

        public TicketService(TicketRepository ticketRepository, TicketListQuery ticketListQuery, CompanyRepository companyRepository, 
            UserRepository userRepository, SeatRepository seatRepository, StopRepository stopRepository)
        {
            Repository = ticketRepository;
            Query = ticketListQuery;
            this.companyRepository = companyRepository;
            this.userRepository = userRepository;
            this.seatRepository = seatRepository;
            this.stopRepository = stopRepository;
        }

        public override int Create(TicketDTOCreate ticketDto)
        {
            using (var uow = UnitOfWorkProvider.Create())
            {
                User user = userRepository.GetById(ticketDto.UserId);
                Stop fromStop = stopRepository.GetById(ticketDto.FromStopID);
                Stop toStop = stopRepository.GetById(ticketDto.ToStopID);

                List<Ticket> tickets = new List<Ticket>();

                foreach(int seatId in ticketDto.SeatIDs)
                {
                    tickets.Add(CreateTicketForOneSeat(ticketDto, user, fromStop, toStop, seatId));
                }

                GetRepository().Insert(tickets);
                uow.Commit();

                // returns a ticket id for one of the created tickets
                return tickets.First().ID;
            }
        }

        private Ticket CreateTicketForOneSeat(TicketDTOCreate ticketDto, User user, Stop fromStop, Stop toStop, int seatId)
        {
            Ticket ticket = Mapper.Map<Ticket>(ticketDto);
            ticket.User = user;
            ticket.From = fromStop;
            ticket.To = toStop;
            ticket.TimeCreated = DateTime.Now;

            Seat seat = seatRepository.GetById(seatId);
            ticket.Seat = seat;
            return ticket;
        }

        public void ReserveTickets(IEnumerable<TicketDTOCreate> ticketDtos)
        {
            foreach (TicketDTOCreate ticketDto in ticketDtos)
            {
                ticketDto.IsPayed = false;
                Create(ticketDto);
            }
        }

        public bool BuyTicket(TicketDTOCreate ticketDto)
        {
            TicketDTO ticketDtoExisting = Get(ticketDto.ID);

            //if ticket is already created, update it
            if (ticketDtoExisting != null)
            {
                //cannot buy ticket that is already payed or canceled
                if (ticketDtoExisting.IsPayed || ticketDtoExisting.IsCanceled)
                {
                    return false;
                }

                ticketDtoExisting.IsPayed = true;

                var updatedTicketDto = Mapper.Map<TicketDTOUpdate>(ticketDtoExisting);
                Update(updatedTicketDto);

                return true;
            }

            ticketDto.IsPayed = true;
            Create(ticketDto);

            return true;
        }

        public bool ReturnTicket(ReturnTicketDTO ticketDto)
        {
            TicketDTO ticketDtoExisting = Get(ticketDto.ID);
             
            using (UnitOfWorkProvider.Create())
            {
                Company company = companyRepository.GetById(ticketDto.CompanyID);

                if (!company.IsTicketReturnAllowed)
                {
                    return false;
                }
            }
            
            ticketDtoExisting.IsCanceled = true;

            return true;
        }

        public void SendEmail(TicketDTO ticketDto)
        {
            string recipientEmail;

            using (UnitOfWorkProvider.Create())
            {
                User user = userRepository.GetById(ticketDto.UserId);
                recipientEmail = user.Email;
            }

            MailMessage mail = new MailMessage(TIMETABLE_MAIL, recipientEmail);
            SmtpClient client = new SmtpClient();
            client.Port = 25;
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            client.UseDefaultCredentials = false;
            client.Host = "smtp.google.com";
            mail.Subject = "Travel contentment form";
            mail.Body = "You can fill your contetnment form relating to the ticket you bought on our website.";
            client.Send(mail);
        }
    }
}
