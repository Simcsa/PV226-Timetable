﻿using BL.DTOs.Filters;
using BL.DTOs.Tickets;
using DAL.Entities;
using System.Collections.Generic;

namespace BL.Services.Tickets
{
    public interface ITicketService : IListCrudServiceBase<Ticket, int, TicketDTOCreate, TicketDTOUpdate, TicketDTO, TicketDTO, TicketListQueryResultDTO, TicketFilter>
    {
        /// <summary>
        /// Reserves the seats, creates tickets for them, marks tickets as not payed.
        /// </summary>
        /// <param name="ticketDtos">TicketDto with seats to be reserved.</param>
        void ReserveTickets(IEnumerable<TicketDTOCreate> ticketDtos);

        /// <summary>
        /// If ticket already exists in form of reservation it marks it as payed, else it creates new.
        /// </summary>
        /// <param name="ticketDto">ticket to be bought</param>
        /// <returns>true if ticket was bought</returns>
        bool BuyTicket(TicketDTOCreate ticketDto);

        /// <summary>
        /// Cancels ticket if possible.
        /// </summary>
        /// <param name="ticketDto">ticket to be cancelled</param>
        /// <returns>true if ticket was cancelled, false otherwise</returns>
        bool ReturnTicket(ReturnTicketDTO ticketDto);

        /// <summary>
        /// Sends the email.
        /// </summary>
        /// <param name="ticketDto">The ticket dto.</param>
        void SendEmail(TicketDTO ticketDto);
    }
}
