﻿using BL.DTOs.Filters;
using BL.DTOs.Users;
using DAL.Entities;

namespace BL.Services.Users
{
    public interface IUserService : IListCrudServiceBase<User, int, UserDTO, UserDTO, UserDTO, UserDTO, UserListQueryResultDTO, UserFilter>
    {
        
    }
}
