﻿using BL.DTOs.Filters;
using BL.DTOs.Users;
using BL.Queries;
using BL.Repositories;
using DAL.Entities;
using Riganti.Utils.Infrastructure.Core;

namespace BL.Services.Users
{
    public class UserService : ListCrudServiceBase<User, int, UserDTO, UserDTO, UserDTO, UserDTO, UserListQueryResultDTO, UserFilter>, IUserService
    {
        private IRepository<User, int> Repository { get; set; }

        private UserListQuery Query { get; set; }

        protected override IRepository<User, int> GetRepository()
        {
            return Repository;
        }

        protected override int GetPageSize()
        {
            return 10;
        }

        protected override IQuery<UserDTO> GetQuery(UserFilter filter)
        {
            Query.Filter = filter;
            return Query;
        }

        public UserService(UserRepository userRepository, UserListQuery userListQuery)
        {
            Repository = userRepository;
            Query = userListQuery;
        }
    }
}
