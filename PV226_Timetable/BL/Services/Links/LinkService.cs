﻿using AutoMapper;
using BL.DTOs.Filters;
using BL.DTOs.Links;
using BL.DTOs.Routes;
using BL.DTOs.TimedStops;
using BL.DTOs.TwoStopRoutes;
using BL.Queries;
using BL.Repositories;
using DAL.Entities;
using Riganti.Utils.Infrastructure.Core;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace BL.Services.Links
{
    public class LinkService : ListCrudServiceBase<Link, int, LinkDTO, LinkDTO, LinkDTO, LinkDTO, LinkListQueryResultDTO, LinkFilter>, ILinkService
    {
        private IRepository<Link, int> Repository { get; set; }

        private readonly RouteRepository routeRepository;
        private readonly VehicleRepository vehicleRepository;
        private readonly TwoStopRouteRepository twoStopRouteRepository;
        private readonly TimedStopRepository timedStopRepository;
        private readonly StopRepository stopRepository;
        private readonly TwoStopRouteListQuery twoStopRouteQuery;

        private LinkListQuery Query { get; set; }

        protected override IRepository<Link, int> GetRepository()
        {
            return Repository;
        }

        protected override IQuery<LinkDTO> GetQuery(LinkFilter filter)
        {
            Query.Filter = filter;
            return Query;
        }

        protected override int GetPageSize()
        {
            return 10;
        }

        public LinkService(LinkRepository linkRepository, LinkListQuery linkListQuery, RouteRepository routeRepository, VehicleRepository vehicleRepository, 
            TwoStopRouteRepository twoStopRouteRepository, TimedStopRepository timedStopRepository, StopRepository stopRepository, TwoStopRouteListQuery twoStopRouteQuery)
        {
            Repository = linkRepository;    
            Query = linkListQuery;
            this.routeRepository = routeRepository;
            this.vehicleRepository = vehicleRepository;
            this.twoStopRouteRepository = twoStopRouteRepository;
            this.timedStopRepository = timedStopRepository;
            this.stopRepository = stopRepository;
            this.twoStopRouteQuery = twoStopRouteQuery;
        }

        public override int Create(LinkDTO linkDto)
        {
            using (var uow = UnitOfWorkProvider.Create())
            {
                Route route = routeRepository.GetById(linkDto.RouteID);
                Vehicle vehicle = vehicleRepository.GetById(linkDto.VehicleID);

                Link link = Mapper.Map<Link>(linkDto);
                link.Route = route;
                link.Vehicle = vehicle;

                GetRepository().Insert(link);

                uow.Commit();

                return link.ID;
            }
        }

        public override void Update(LinkDTO linkDto)
        {
            using (var uow = UnitOfWorkProvider.Create())
            {
                Route route = routeRepository.GetById(linkDto.RouteID);
                Vehicle vehicle = vehicleRepository.GetById(linkDto.VehicleID);

                Link link = Mapper.Map<Link>(linkDto);
                link.Route = route;
                link.Vehicle = vehicle;

                GetRepository().Update(link);

                uow.Commit();
            }
        }

        public override LinkDTO Get(int id)
        {
            using (UnitOfWorkProvider.Create())
            {
                var link = GetRepository().GetById(id, includes: l => l.Route);

                var linkDto = Mapper.Map<LinkDTO>(link);
                linkDto.Route.TwoStopRoutes = GetTwoStopRoutes(linkDto);

                CountLeaveAndArriveTimes(linkDto);

                return linkDto;
            }
        }

        public override IEnumerable<LinkDTO> List(LinkFilter filter)
        {
            IEnumerable<LinkDTO> links = base.List(filter);

            using (UnitOfWorkProvider.Create())
            {
                foreach (LinkDTO linkDTO in links)
                {
                    linkDTO.Route.TwoStopRoutes = GetTwoStopRoutes(linkDTO);
                }
            }

            //for now try search only for direct links
            if (!String.IsNullOrEmpty(filter.LeaveStopName) && !String.IsNullOrEmpty(filter.ArriveStopName))
            {
                links = links.Where(link =>
                    // if it is the right direction (the link goes from leave stop to arrive stop not the other way around)
                    link.Route.TwoStopRoutes
                    .Where(
                        route => route.LeaveStop.Stop.Name.ToLower().Contains(
                            filter.LeaveStopName.ToLower()) 
                        || route.ArriveStop.Stop.Name.ToLower().Contains(
                            filter.ArriveStopName.ToLower())
                    ).FirstOrDefault().LeaveStop.Stop.Name.ToLower().Contains(
                        filter.LeaveStopName.ToLower()));
            }

            return links;
        }

        private List<TwoStopRoute> GetTwoStopRoutes(LinkDTO linkDto)
        {
            twoStopRouteQuery.Filter = new TwoStopRouteFilter() { RouteId = linkDto.Route.ID };
            IEnumerable<TwoStopRouteDTO> twoStopRouteDTOs = twoStopRouteQuery.Execute();
            List<TwoStopRoute> twoStopRoutes = new List<TwoStopRoute>();

            foreach (TwoStopRouteDTO twoStopDto in twoStopRouteDTOs)
            {
                TwoStopRoute twoStop = Mapper.Map<TwoStopRoute>(twoStopDto);
                twoStop.ArriveStop = timedStopRepository.GetById(twoStopDto.ArriveStopId, includes: ts => ts.Stop);
                twoStop.LeaveStop = timedStopRepository.GetById(twoStopDto.LeaveStopId, includes: ts => ts.Stop);
                twoStopRoutes.Add(twoStop);
            }

            return twoStopRoutes;
        }

        public void CountLeaveAndArriveTimes(LinkDTO linkDto)
        {
            var routes = linkDto.Route.TwoStopRoutes;
            List<LeaveAndArriveTimeDTO> timedStops = new List<LeaveAndArriveTimeDTO>();

            // leave time from first stop is the same as leave time of the link
            DateTime time = linkDto.LeaveTime;
            LeaveAndArriveTimeDTO timedStopDto = new LeaveAndArriveTimeDTO { LeaveTime = time };
            timedStops.Add(timedStopDto);

            for (int i = 0; i < routes.Count - 1; i++)
            {
                var twoStopRoute = routes.ElementAt(i);

                LeaveAndArriveTimeDTO dto = new LeaveAndArriveTimeDTO { };

                time = time.Add(twoStopRoute.TravelTime);
                dto.ArriveTime = time;

                time = time.Add(twoStopRoute.ArriveStop.StopTime);
                dto.LeaveTime = time;

                timedStops.Add(dto);
            }

            // last stop does not contain leaveTime
            var lastTwoStopRoute = routes.ElementAt(routes.Count - 1);
            time = time.Add(lastTwoStopRoute.TravelTime);
            LeaveAndArriveTimeDTO lastDto = new LeaveAndArriveTimeDTO { ArriveTime = time };
            timedStops.Add(lastDto);

            linkDto.Route.LeaveAndArriveTimes = timedStops;
        }
    }
}
