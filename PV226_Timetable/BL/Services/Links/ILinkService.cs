﻿using BL.DTOs.Filters;
using BL.DTOs.Links;
using DAL.Entities;

namespace BL.Services.Links
{
    public interface ILinkService : IListCrudServiceBase<Link, int, LinkDTO, LinkDTO, LinkDTO, LinkDTO, LinkListQueryResultDTO, LinkFilter>
    {
    }
}
