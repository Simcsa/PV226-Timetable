﻿namespace BL.Services
{
    public interface ICrudServiceBase<TEntity, TKey, TCreateDTO, TUpdateDTO, TDetailDTO>
    {
        /// <summary>
        /// Creates a new entity using <paramref name="dto"/>.
        /// </summary>
        /// <param name="dto">entity details</param>
        TKey Create(TCreateDTO dto);
        
        /// <summary>
        /// Updates an entity using <paramref name="dto"/>.
        /// </summary>
        /// <param name="dto">entity details</param>
        void Update(TUpdateDTO dto);

        /// <summary>
        /// Deletes an entity with <paramref name="id"/>.
        /// </summary>
        /// <param name="id">entity id</param>
        void Delete(TKey id);

        /// <summary>
        /// Gets <see cref="TEntity"/> by <paramref name="id"/> and returns it as <see cref="TDetailDTO"/>.
        /// </summary>
        /// <param name="id">entity id</param>
        /// <returns><see cref="TDetailDTO"/> created from <see cref="TEntity"/></returns>
        TDetailDTO Get(TKey id);
    }
}
