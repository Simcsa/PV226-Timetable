﻿using BL.DTOs.Filters;
using BL.DTOs.TwoStopRoutes;
using DAL.Entities;

namespace BL.Services.TwoStopRoutes
{
    public interface ITwoStopRouteService : ICrudServiceBase<TwoStopRoute, int, TwoStopRouteDTO, TwoStopRouteDTO, TwoStopRouteDTO>
    {
    }
}
