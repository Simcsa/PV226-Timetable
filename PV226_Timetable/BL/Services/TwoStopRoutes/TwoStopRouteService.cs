﻿using AutoMapper;
using BL.DTOs.Filters;
using BL.DTOs.TwoStopRoutes;
using BL.Queries;
using BL.Repositories;
using DAL.Entities;
using Riganti.Utils.Infrastructure.Core;

namespace BL.Services.TwoStopRoutes
{
    public class TwoStopRouteService : CrudServiceBase<TwoStopRoute, int, TwoStopRouteDTO, TwoStopRouteDTO, TwoStopRouteDTO>, ITwoStopRouteService
    {
        private IRepository<TwoStopRoute, int> Repository { get; set; }

        private readonly TimedStopRepository timedStopRepository;
        private readonly RouteRepository routeRepository;

        private TwoStopRouteListQuery Query { get; set; }

        protected override IRepository<TwoStopRoute, int> GetRepository()
        {
            return Repository;
        }

        public TwoStopRouteService(TwoStopRouteRepository twoStopRouteRepository, TwoStopRouteListQuery query, TimedStopRepository timedStopRepository, RouteRepository routeRepository)
        {
            Repository = twoStopRouteRepository;
            Query = query;
            this.timedStopRepository = timedStopRepository;
            this.routeRepository = routeRepository;
        }

        public override int Create(TwoStopRouteDTO twoStopDto)
        {
            using (var uow = UnitOfWorkProvider.Create())
            {
                Route route = routeRepository.GetById(twoStopDto.RouteId);
                TimedStop leaveStop = timedStopRepository.GetById(twoStopDto.LeaveStopId);
                TimedStop arriveStop = timedStopRepository.GetById(twoStopDto.ArriveStopId);

                TwoStopRoute twoStopRoute = Mapper.Map<TwoStopRoute>(twoStopDto);
                twoStopRoute.LeaveStop = leaveStop;
                twoStopRoute.ArriveStop = arriveStop;
                twoStopRoute.Route = route;

                GetRepository().Insert(twoStopRoute);

                uow.Commit();

                return twoStopRoute.ID;
            }
        }
    }
}
