﻿using BL.DTOs.TimedStops;
using DAL.Entities;

namespace BL.Services.TimedStops
{
    public interface ITimedStopService : ICrudServiceBase<TimedStop, int, TimedStopDTO, TimedStopDTO, TimedStopDTO>
    {
    }
}
