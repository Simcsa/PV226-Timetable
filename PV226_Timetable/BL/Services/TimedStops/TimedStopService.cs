﻿using AutoMapper;
using BL.DTOs.TimedStops;
using BL.Repositories;
using DAL.Entities;
using Riganti.Utils.Infrastructure.Core;

namespace BL.Services.TimedStops
{
    public class TimedStopService : CrudServiceBase<TimedStop, int, TimedStopDTO, TimedStopDTO, TimedStopDTO>, ITimedStopService
    {
        private IRepository<TimedStop, int> Repository { get; set; }

        private StopRepository stopRepository;

        protected override IRepository<TimedStop, int> GetRepository()
        {
            return Repository;
        }

        public TimedStopService(TimedStopRepository timedStopRepository, StopRepository stopRepository)
        {
            Repository = timedStopRepository;
            this.stopRepository = stopRepository;
        }

        public override int Create(TimedStopDTO timedStopDTO)
        {
            using (var uow = UnitOfWorkProvider.Create())
            {
                Stop stop = stopRepository.GetById(timedStopDTO.StopId);

                TimedStop timedStop = Mapper.Map<TimedStop>(timedStopDTO);
                timedStop.Stop = stop;

                GetRepository().Insert(timedStop);

                uow.Commit();

                return timedStop.ID;
            }
        }
    }
}
