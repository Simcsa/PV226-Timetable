﻿using AutoMapper;
using BL.DTOs.Filters;
using BL.DTOs.Forms;
using BL.Queries;
using BL.Repositories;
using DAL.Entities;
using Riganti.Utils.Infrastructure.Core;

namespace BL.Services.Forms
{
    public class FormService : ListCrudServiceBase<Form, int, FormDTOCreate, FormDTOCreate, FormDTO, FormDTO, FormListQueryResultDTO, FormFilter>, IFormService
    {
        private IRepository<Form, int> Repository { get; set; }

        private readonly TicketRepository ticketRepository;

        private FormListQuery Query { get; set; }

        protected override IRepository<Form, int> GetRepository()
        {
            return Repository;
        }

        protected override IQuery<FormDTO> GetQuery(FormFilter filter)
        {
            Query.Filter = filter;
            return Query;
        }

        protected override int GetPageSize()
        {
            return 10;
        }

        public FormService(FormRepository formRepository, FormListQuery formListQuery, TicketRepository ticketRepository)
        {
            Repository = formRepository;
            Query = formListQuery;
            this.ticketRepository = ticketRepository;
        }

        public override int Create(FormDTOCreate formDto)
        {
            using (var uow = UnitOfWorkProvider.Create())
            {
                Ticket ticket = ticketRepository.GetById(formDto.TicketID);

                Form form = Mapper.Map<Form>(formDto);
                form.Ticket = ticket;

                GetRepository().Insert(form);

                uow.Commit();

                return form.ID;
            }
        }
    }
}
