﻿using BL.DTOs.Filters;
using BL.DTOs.Forms;
using DAL.Entities;

namespace BL.Services.Forms
{
    public interface IFormService : IListCrudServiceBase<Form, int, FormDTOCreate, FormDTOCreate, FormDTO, FormDTO, FormListQueryResultDTO, FormFilter>
    {
       
    }
}
