﻿using System.Collections.Generic;
using Riganti.Utils.Infrastructure.Core;
using BL.DTOs.Common;
using System;

namespace BL.Services
{
    public abstract class ListCrudServiceBase<TEntity, TKey, TCreateDTO, TUpdateDTO, TDetailDTO, TListDTO, TPagedListDTO, TQueryFilter>
        : CrudServiceBase<TEntity, TKey, TCreateDTO, TUpdateDTO, TDetailDTO>, IListCrudServiceBase<TEntity, TKey, TCreateDTO, TUpdateDTO, TDetailDTO, TListDTO, TPagedListDTO, TQueryFilter>
        where TEntity : IEntity<TKey>
        where TDetailDTO : DTOBase<TKey>
        where TUpdateDTO : DTOBase<TKey>
        where TPagedListDTO : PagedListQueryResultDTO<TListDTO>, new()
    {
        protected abstract IQuery<TListDTO> GetQuery(TQueryFilter filter);

        protected abstract int GetPageSize();

        public virtual IEnumerable<TListDTO> List(TQueryFilter filter)
        {
            using (UnitOfWorkProvider.Create())
            {
                var query = GetQuery(filter);
                return query.Execute() ?? new List<TListDTO>();
            }
        }

        public TPagedListDTO List(TQueryFilter filter, int requiredPage)
        {
            using (UnitOfWorkProvider.Create())
            {
                var query = GetQuery(filter);
                query.Take = GetPageSize();
                query.Skip = Math.Max(0, requiredPage - 1) * GetPageSize();

                return new TPagedListDTO
                {
                    RequestedPage = requiredPage,
                    TotalResultCount = query.GetTotalRowCount(),
                    ResultsPage = query.Execute()
                };
            }
        }
    }
}
