﻿using BL.DTOs.Routes;
using DAL.Entities;

namespace BL.Services.Routes
{
    public interface IRouteService : ICrudServiceBase<Route, int, RouteDTO, RouteDTO, RouteDTO>
    {
        
    }
}
