﻿using BL.DTOs.Routes;
using BL.DTOs.TwoStopRoutes;
using BL.Repositories;
using DAL.Entities;
using Riganti.Utils.Infrastructure.Core;

namespace BL.Services.Routes
{
    public class RouteService : CrudServiceBase<Route, int, RouteDTO, RouteDTO, RouteDTO>, IRouteService
    {
        private IRepository<Route, int> Repository { get; set; }

        private readonly TwoStopRouteRepository twoStopRouteRepository;

        protected override IRepository<Route, int> GetRepository()
        {
            return Repository;
        }

        public RouteService(RouteRepository routeRepository, TwoStopRouteRepository twoStopRouteRepository)
        {
            Repository = routeRepository;
            this.twoStopRouteRepository = twoStopRouteRepository;
        }
    }
}
