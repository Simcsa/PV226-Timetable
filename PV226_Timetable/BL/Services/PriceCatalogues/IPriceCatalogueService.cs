﻿using BL.DTOs.Discounts;
using BL.DTOs.PriceCatalogues;
using DAL.Entities;

namespace BL.Services.PriceCatalogues
{
    public interface IPriceCatalogueService : ICrudServiceBase<PriceCatalogue, int, PriceCatalogueDTO, PriceCatalogueDTO, PriceCatalogueDTO>
    {
        /// <summary>
        /// Creates a discount for price catalogue specified by price catalogue id property of <paramref name="discountDTO"/>.
        /// </summary>
        /// <param name="discountDTO">The discount dto with price catalogue id.</param>
        /// <returns>true if discount was added, false if there is already such discount type</returns>
        bool AddDiscount(DiscountCreateDTO discountDTO);

        /// <summary>
        /// Removes the discount for specified catalogue and ticket type.
        /// </summary>
        /// <param name="discountDTO">The discount dto with price catalogue id and ticket type to remove.</param>
        /// <returns>true if discount was removed</returns>
        bool RemoveDiscount(DiscountCreateDTO discountDTO);

        /// <summary>
        /// Updates the discount for specified catalogue and ticket type.
        /// </summary>
        /// <param name="discountDTO">The discount dto with price catalogue id.</param>
        /// <returns>true if discount was updated</returns>
        bool UpdateDiscount(DiscountCreateDTO discountDTO);
    }
}
