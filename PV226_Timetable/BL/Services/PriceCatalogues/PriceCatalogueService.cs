﻿using System;
using System.Linq;
using AutoMapper;
using BL.DTOs.Discounts;
using BL.DTOs.PriceCatalogues;
using BL.Repositories;
using DAL.Entities;
using Riganti.Utils.Infrastructure.Core;

namespace BL.Services.PriceCatalogues
{
    public class PriceCatalogueService : CrudServiceBase<PriceCatalogue, int, PriceCatalogueDTO,  PriceCatalogueDTO, PriceCatalogueDTO>, IPriceCatalogueService
    {
        private IRepository<PriceCatalogue, int> Repository { get; set; }

        private readonly DiscountRepository discountRepository;

        protected override IRepository<PriceCatalogue, int> GetRepository()
        {
            return Repository;
        }

        public PriceCatalogueService(PriceCatalogueRepository priceCatalogueRepository, DiscountRepository discountRepository)
        {
            Repository = priceCatalogueRepository;
            this.discountRepository = discountRepository;
        }

        public override PriceCatalogueDTO Get(int id)
        {
            using (var uow = UnitOfWorkProvider.Create())
            {
                var priceCatalogue = Repository.GetById(id, pc => pc.Discounts);
                return Mapper.Map<PriceCatalogueDTO>(priceCatalogue);
            }
        }

        public bool AddDiscount(DiscountCreateDTO discountDTO)
        {
            using (var uow = UnitOfWorkProvider.Create())
            {
                PriceCatalogue priceCatalogue = Repository.GetById(discountDTO.PriceCatalogueId, pc => pc.Discounts);

                // if price catalogue already contains such discount type, do not add it again
                if (priceCatalogue.Discounts.Any(d => d.TicketType == discountDTO.TicketType))
                {
                    return false;
                }

                Discount discount = Mapper.Map<Discount>(discountDTO);
                discountRepository.Insert(discount);

                priceCatalogue.Discounts.Add(discount);
                Repository.Update(priceCatalogue);

                uow.Commit();

                return true;
            }
        }

        public bool RemoveDiscount(DiscountCreateDTO discountDTO)
        {
            using (var uow = UnitOfWorkProvider.Create())
            {
                PriceCatalogue priceCatalogue = Repository.GetById(discountDTO.PriceCatalogueId, pc => pc.Discounts);

                // if price catalogue contains such discount type, remove it
                if (priceCatalogue.Discounts.Any(d => d.TicketType == discountDTO.TicketType))
                {
                    Discount discount = priceCatalogue.Discounts.Where(d => d.TicketType == discountDTO.TicketType).FirstOrDefault();
                    discountRepository.Delete(discount);

                    uow.Commit();
                    return true;
                }

                return false;
            }
        }

        public bool UpdateDiscount(DiscountCreateDTO discountDTO)
        {
            using (var uow = UnitOfWorkProvider.Create())
            {
                PriceCatalogue priceCatalogue = Repository.GetById(discountDTO.PriceCatalogueId, pc => pc.Discounts);

                // if price catalogue contains such discount type, update it
                if (priceCatalogue.Discounts.Any(d => d.TicketType == discountDTO.TicketType))
                {
                    Discount discount = priceCatalogue.Discounts.Where(d => d.TicketType == discountDTO.TicketType).FirstOrDefault();
                    discount.DiscountPercentage = discountDTO.DiscountPercentage;
                    discountRepository.Update(discount);

                    uow.Commit();
                    return true;
                }

                return false;
            }
        }
    }
}
