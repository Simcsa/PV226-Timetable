﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL.Utils
{
    /// <summary>
    /// Defines various roles used within authentication
    /// </summary>
    public static class Claims
    {
        public const string Customer = "Customer";

        public const string Admin = "Administrator";

        public const string AuthenticatedUsers = "Customer, Administrator";
    }
}
