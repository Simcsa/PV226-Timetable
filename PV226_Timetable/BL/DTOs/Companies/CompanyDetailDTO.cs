﻿using System.ComponentModel.DataAnnotations;
using BL.DTOs.Common;

namespace BL.DTOs.Companies
{
    public class CompanyDetailDTO : DTOBase<int>
    {
        [Required]
        [MaxLength(256)]
        public string Name { get; set; }

        [Display(Name = "Ticket return")]
        public bool IsTicketReturnAllowed { get; set; }

        [Required]
        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        [Display(Name = "Price for 1 km")]
        public decimal PricePerKm { get; set; }

        public int? PriceCatalogueId { get; set; }
    }
}
