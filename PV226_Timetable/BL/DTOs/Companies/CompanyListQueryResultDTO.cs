﻿using BL.DTOs.Common;

namespace BL.DTOs.Companies
{
    public class CompanyListQueryResultDTO : PagedListQueryResultDTO<CompanyListDTO> { }
}
