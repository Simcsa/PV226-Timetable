﻿using System;
using System.ComponentModel.DataAnnotations;
using BL.DTOs.Common;
using BL.DTOs.Routes;
using DAL.Entities;

namespace BL.DTOs.Links
{
    public class LinkDTO : DTOBase<int>
    {
        [Required]
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd hh:mm:ss}", ApplyFormatInEditMode = true)]
        [Display(Name = "Leave time")]
        public DateTime LeaveTime { get; set; }

        public TimeSpan Delay { get; set; }

        public RouteDTO Route { get; set; }

        public int RouteID { get; set; }

        public int VehicleID { get; set; }

        [Display(Name = "Vehicle type")]
        public VehicleType VehicleType { get; set; }
    }
}