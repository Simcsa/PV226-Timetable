﻿using BL.DTOs.Common;

namespace BL.DTOs.Links
{
    public class LinkListQueryResultDTO : PagedListQueryResultDTO<LinkDTO>
    {
    }
}
