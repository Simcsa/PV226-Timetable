﻿using System.Collections.Generic;
using BL.DTOs.Common;
using DAL.Entities;
using BL.DTOs.TimedStops;
using Newtonsoft.Json;

namespace BL.DTOs.Routes
{
    public class RouteDTO : DTOBase<int>
    {
        public List<TwoStopRoute> TwoStopRoutes { get; set; }

        [JsonIgnore]
        public List<LeaveAndArriveTimeDTO> LeaveAndArriveTimes { get; set; }
    }
}