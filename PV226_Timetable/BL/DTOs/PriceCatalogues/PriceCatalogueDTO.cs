﻿
using BL.DTOs.Common;
using BL.DTOs.Discounts;
using System.Collections.Generic;

namespace BL.DTOs.PriceCatalogues
{
    public class PriceCatalogueDTO : DTOBase<int>
    {
        public decimal PricePerKm { get; set; }

        public HashSet<DiscountDetailDTO> Discounts { get; set; }
    }
}