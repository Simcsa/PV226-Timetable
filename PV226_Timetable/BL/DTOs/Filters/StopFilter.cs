﻿namespace BL.DTOs.Filters
{
    public class StopFilter
    {
        public string Name { get; set; }

        public string Town { get; set; }
    }
}
