﻿using System;

namespace BL.DTOs.Filters
{
    public class SeatFilter
    {
        public int Number { get; set; }

        public bool? IsOccupied { get; set; }

        public int LinkId { get; set; }
    }
}
