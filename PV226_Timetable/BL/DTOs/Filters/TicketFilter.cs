﻿using System;
using System.Collections.Generic;
using BL.Utils;
using DAL.Entities;

namespace BL.DTOs.Filters
{
    public class TicketFilter
    {
        public int UserId { get; set; }

        public string UserFirstName { get; set; }

        public string UserLastName { get; set; }

        public HashSet<VehicleType> VehicleTypes { get; set; }

        public int SeatNumber { get; set; }

        public int SeatId { get; set; }

        public string FromStopName { get; set; }

        public string ToStopName { get; set; }

        public bool? IsPayed { get; set; }

        public bool? IsCanceled { get; set; }

        public Range<int> Price { get; set; }

        public Range<DateTime> TimeCreated { get; set; }
    }
}
