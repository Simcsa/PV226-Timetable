﻿using BL.Utils;

namespace BL.DTOs.Filters
{
    public class FormFilter
    {
        public int UserId { get; set; }

        public string UserFirstName { get; set; }

        public string UserLastName { get; set; }

        public Range<int> Rating { get; set; }
    }
}
