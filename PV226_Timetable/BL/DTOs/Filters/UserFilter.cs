﻿namespace BL.DTOs.Filters
{
    public class UserFilter
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Email { get; set; }
    }
}
