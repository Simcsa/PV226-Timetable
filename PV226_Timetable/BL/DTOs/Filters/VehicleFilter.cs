﻿using System.Collections.Generic;
using BL.Utils;
using DAL.Entities;

namespace BL.DTOs.Filters
{
    public class VehicleFilter
    {
        public HashSet<VehicleType> VehicleTypes { get; set; }

        public Range<int> Capacity { get; set; }

        public string CompanyName { get; set; }
    }
}