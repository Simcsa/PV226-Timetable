﻿using System;
using System.Collections.Generic;
using DAL.Entities;
using Microsoft.Build.Framework;

namespace BL.DTOs.Filters
{
    public class LinkFilter
    {
        public DateTime LeaveTime { get; set; }

        public HashSet<VehicleType> VehicleTypes { get; set; }

        [Required]
        public string LeaveStopName { get; set; }

        [Required]
        public string ArriveStopName { get; set; }
    }
}
