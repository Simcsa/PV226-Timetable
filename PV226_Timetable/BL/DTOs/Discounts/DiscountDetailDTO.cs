﻿using System.ComponentModel.DataAnnotations;
using DAL.Entities;
using BL.DTOs.Common;

namespace BL.DTOs.Discounts
{
    public class DiscountDetailDTO : DTOBase<int>
    {
        [Required]
        [Display(Name = "Ticket type")]
        public TicketType TicketType { get; set; }

        [Required]
        public int DiscountPercentage { get; set; }
    }
}