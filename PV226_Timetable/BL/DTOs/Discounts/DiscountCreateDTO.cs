﻿using BL.DTOs.Common;
using DAL.Entities;
using System.ComponentModel.DataAnnotations;

namespace BL.DTOs.Discounts
{
    public class DiscountCreateDTO : DTOBase<int>
    {
        [Required]
        [Display(Name = "Ticket type")]
        public TicketType TicketType { get; set; }

        [Required]
        [Range(1, 100)]
        [Display(Name = "Discount percentage")]
        public int DiscountPercentage { get; set; }

        public int PriceCatalogueId { get; set; }
    }
}
