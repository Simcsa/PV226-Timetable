﻿using BL.DTOs.Common;

namespace BL.DTOs.Seats
{
    public class SeatListQueryResultDTO : PagedListQueryResultDTO<SeatDTO>
    {
    }
}
