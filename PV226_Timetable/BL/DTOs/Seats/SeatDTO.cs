﻿using System.ComponentModel.DataAnnotations;
using BL.DTOs.Common;

namespace BL.DTOs.Seats
{
    public class SeatDTO : DTOBase<int>
    {
        [Required]
        public int Number { get; set; }

        public int LinkID { get; set; }
    }
}