﻿using System.ComponentModel.DataAnnotations;
using DAL.Entities;
using BL.DTOs.Common;

namespace BL.DTOs.Vehicles
{
    public class VehicleDTO : DTOBase<int>
    {
        public VehicleType VehicleType { get; set; }

        [Required]
        public int Capacity { get; set; }

        [Required]
        public string CompanyName { get; set; }
    }
}