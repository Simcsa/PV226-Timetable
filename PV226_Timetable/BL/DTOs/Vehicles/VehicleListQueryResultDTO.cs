﻿using BL.DTOs.Common;

namespace BL.DTOs.Vehicles
{
    public class VehicleListQueryResultDTO : PagedListQueryResultDTO<VehicleDTO>
    {
    }
}
