﻿using System.ComponentModel.DataAnnotations;
using DAL.Entities;
using BL.DTOs.Common;

namespace BL.DTOs.Vehicles
{
    public class VehicleDTOCreate : DTOBase<int>
    {
        public VehicleType VehicleType { get; set; }

        [Required]
        public int Capacity { get; set; }

        [Required]
        public int CompanyId { get; set; }
    }
}