﻿using BL.DTOs.Common;

namespace BL.DTOs.Stops
{
    public class StopListQueryResultDTO : PagedListQueryResultDTO<StopDTO>
    {
    }
}
