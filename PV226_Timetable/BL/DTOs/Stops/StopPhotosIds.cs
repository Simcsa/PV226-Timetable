﻿using System.Collections.Generic;

namespace BL.DTOs.Stops
{
    public class StopPhotosIds
    {
        public List<int> PhotosIds { get; set; }
    }
}
