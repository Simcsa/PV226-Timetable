﻿using BL.DTOs.Common;
using DAL.Entities;
using System.Collections.Generic;

namespace BL.DTOs.Stops
{
    public class StopDTOWihPhotos : DTOBase<int>
    {
        public List<Photo> Photos { get; set; } 
    }
}
