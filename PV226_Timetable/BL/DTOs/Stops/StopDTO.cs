﻿using System.ComponentModel.DataAnnotations;
using BL.DTOs.Common;

namespace BL.DTOs.Stops
{
    public class StopDTO : DTOBase<int>
    {
        [Required]
        public string Name { get; set; }

        public string Town { get; set; }

        public override string ToString()
        {
            return string.IsNullOrEmpty(Town) ? $"{Name}" : $"{Name}, {Town}";
        }
    }
}