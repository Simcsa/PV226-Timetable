﻿using System;
using System.ComponentModel.DataAnnotations;

namespace BL.DTOs.TimedStops
{
    public class LeaveAndArriveTimeDTO
    {
        [DisplayFormat(DataFormatString = "{0:h:mm}")]
        public DateTime? ArriveTime { get; set; }

        [DisplayFormat(DataFormatString = "{0:h:mm}")]
        public DateTime? LeaveTime { get; set; }
    }
}
