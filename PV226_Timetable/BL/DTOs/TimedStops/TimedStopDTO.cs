﻿using BL.DTOs.Common;
using System;
using System.ComponentModel.DataAnnotations;

namespace BL.DTOs.TimedStops
{
    public class TimedStopDTO : DTOBase<int>
    {
        [Required]
        public int StopId { get; set; }

        [Required]
        public TimeSpan StopTime { get; set; }
    }
}