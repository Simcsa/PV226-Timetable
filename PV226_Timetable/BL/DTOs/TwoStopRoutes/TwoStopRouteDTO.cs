﻿using System;
using System.ComponentModel.DataAnnotations;
using BL.DTOs.Common;

namespace BL.DTOs.TwoStopRoutes
{
    public class TwoStopRouteDTO : DTOBase<int>
    {
        public TimeSpan TravelTime { get; set; }

        [Required]
        public decimal LengthInKm { get; set; }

        public int LeaveStopId { get; set; }
        
        public int ArriveStopId { get; set; }

        public int RouteId { get; set; }

        public int Order { get; set; }
    }
}