﻿using System.ComponentModel.DataAnnotations;
using BL.DTOs.Common;

namespace BL.DTOs.Users
{
    public class UserDTO : DTOBase<int>
    {
        [Required]
        public string FirstName { get; set; }

        [Required]
        public string LastName { get; set; }

        [Required]
        public string Email { get; set; }
    }
}