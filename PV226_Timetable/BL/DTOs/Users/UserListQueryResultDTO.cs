﻿using BL.DTOs.Common;

namespace BL.DTOs.Users
{
    public class UserListQueryResultDTO : PagedListQueryResultDTO<UserDTO>
    {
    }
}
