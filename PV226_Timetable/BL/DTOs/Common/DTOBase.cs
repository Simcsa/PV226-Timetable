﻿namespace BL.DTOs.Common
{
    public class DTOBase<TKey>
    {
        public TKey ID { get; set; }
    }
}
