﻿using BL.DTOs.Common;

namespace BL.DTOs.Tickets
{
    public class TicketDTO : DTOBase<int>
    {
        public decimal Price { get; set; }
        
        public bool IsPayed { get; set; }

        public bool IsCanceled { get; set; }

        public int SeatNumber { get; set; }

        public string FromStopName { get; set; }

        public string ToStopName { get; set; }

        public int UserId { get; set; }

        public int? FormID { get; set; }
        
        public int SeatID { get; set; }

        public int FromStopID { get; set; }

        public int ToStopID { get; set; }
    }
}