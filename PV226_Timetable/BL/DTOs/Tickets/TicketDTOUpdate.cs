﻿using BL.DTOs.Common;

namespace BL.DTOs.Tickets
{
    public class TicketDTOUpdate : DTOBase<int>
    {
        public bool IsPayed { get; set; }

        public bool IsCanceled { get; set; }

        public int? FormID { get; set; }
    }
}