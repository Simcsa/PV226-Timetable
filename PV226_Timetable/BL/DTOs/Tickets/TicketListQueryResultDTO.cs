﻿using BL.DTOs.Common;

namespace BL.DTOs.Tickets
{
    public class TicketListQueryResultDTO : PagedListQueryResultDTO<TicketDTO>
    {
    }
}
