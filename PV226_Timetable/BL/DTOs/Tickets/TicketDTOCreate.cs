﻿using BL.DTOs.Common;
using System.Collections.Generic;

namespace BL.DTOs.Tickets
{
    public class TicketDTOCreate : DTOBase<int>
    {
        public decimal Price { get; set; }

        public bool IsPayed { get; set; }

        public int UserId { get; set; }

        public IEnumerable<int> SeatIDs { get; set; }

        public int FromStopID { get; set; }

        public int ToStopID { get; set; }
    }
}