﻿using BL.DTOs.Common;

namespace BL.DTOs.Tickets
{
    public class ReturnTicketDTO : DTOBase<int>
    {
        public int CompanyID { get; set; }
    }
}
