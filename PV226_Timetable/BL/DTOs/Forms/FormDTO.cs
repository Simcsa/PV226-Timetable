﻿using System.ComponentModel.DataAnnotations;
using BL.DTOs.Common;

namespace BL.DTOs.Forms
{
    public class FormDTO : DTOBase<int>
    {
        [Required]
        public string UserFirstName { get; set; }

        [Required]
        public string UserLastName { get; set; }

        [Required]
        public int Rating { get; set; }

        public string Comment { get; set; }
    }
}