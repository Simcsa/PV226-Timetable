﻿using BL.DTOs.Common;
using Microsoft.Build.Framework;

namespace BL.DTOs.Forms
{
    public class FormDTOCreate : DTOBase<int>
    {
        [Required]
        public int Rating { get; set; }

        public string Comment { get; set; }

        public int TicketID { get; set; }
    }
}
