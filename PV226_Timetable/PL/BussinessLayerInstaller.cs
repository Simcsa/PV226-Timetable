﻿using System;
using System.Data.Entity;
using BL.AppInfrastructure;
using BL.DTOs.Companies;
using BL.DTOs.Filters;
using BL.Facades;
using BL.Services;
using BL.Services.Companies;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.Resolvers.SpecializedResolvers;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using DAL;
using DAL.Entities;
using Riganti.Utils.Infrastructure.Core;
using Riganti.Utils.Infrastructure.EntityFramework;

namespace PL
{
    public class BussinessLayerInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(
                Component.For<Func<DbContext>>()
                    .Instance(() => new TimetableDbContext())
                    .LifestyleTransient(),

                Component.For<IUnitOfWorkProvider>()
                    .ImplementedBy<AppUnitOfWorkProvider>()
                    .LifestyleSingleton(),

                Component.For<IUnitOfWorkRegistry>()
                    .Instance(new HttpContextUnitOfWorkRegistry(new ThreadLocalUnitOfWorkRegistry()))
                    .LifestyleSingleton(),

                Component.For(typeof(IRepository<,>))
                    .ImplementedBy(typeof(EntityFrameworkRepository<,>))
                    .LifestyleTransient(),

                Classes.FromAssemblyContaining<AppUnitOfWork>()
                    .BasedOn(typeof(AppQuery<>))
                    .LifestyleTransient(),

                Classes.FromAssemblyContaining<AppUnitOfWork>()
                    .BasedOn(typeof(IRepository<,>))
                    .LifestyleTransient(),
                /*
                Classes.FromThisAssembly()
                    .BasedOn(typeof(ICrudServiceBase<,,,,>))
                    .WithServiceDefaultInterfaces()
                    .LifestyleTransient(),

                Classes.FromThisAssembly()
                    .BasedOn(typeof(IListCrudServiceBase<,,,,,,>))
                    .WithServiceDefaultInterfaces()
                    .LifestyleTransient(),*/

                Classes.FromThisAssembly()
                    .BasedOn(typeof(ICompanyService))
                    .WithServiceDefaultInterfaces()
                    .LifestyleTransient(),


                Component.For(typeof(CompanyFacade))
                    .ImplementedBy(typeof(CompanyFacade))
                    .DependsOn(Dependency.OnComponent< CompanyService, IListCrudServiceBase < Company, int, CompanyDetailDTO, CompanyDetailDTO, CompanyDetailDTO, CompanyListDTO, CompanyListQueryResultDTO, CompanyFilter>>())
                    .LifestyleTransient()
            );
            container.Kernel.Resolver.AddSubResolver(new CollectionResolver(container.Kernel));
        }
    }
}