﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using BL.DTOs.Filters;
using BL.DTOs.Tickets;
using BL.Facades;
using PL.Models;

namespace PL.Controllers
{
    [Authorize]
    public class TicketController : Controller
    {
        public TicketFacade TicketFacade { get; set; }
        public LinkFacade LinkFacade { get; set; }
        public StopFacade StopFacade { get; set; }

        // GET: Ticket
        public ActionResult Index(string from, string to)
        {
            var filter = new TicketFilter()
            {
                FromStopName = from,
                ToStopName = to
            };

            IEnumerable<TicketDTO> listDtos = TicketFacade.List(filter);
            var model = new TicketListViewModel()
            {
                Tickets = listDtos,
            };
            return View("TicketListView", model);
        }

        // GET: Ticket/Details/5
        public ActionResult Details(int id)
        {
            TicketDTO ticketDTO = TicketFacade.GetTicket(id);
            return View("TicketDetailView", ticketDTO);
        }

        // GET: Ticket/Create
        public ActionResult Create()
        {
            var list = StopFacade.List(new StopFilter());
            ViewBag.ShowDropDown = new SelectList(list, "ID", "Name");

            return View("TicketCreate", new TicketDTOCreate());
        }

        // POST: Ticket/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Ticket/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Ticket/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Ticket/Delete/5
        public ActionResult Delete(int id)
        {
            TicketDTO ticketDTO = TicketFacade.GetTicket(id);

            bool canBeCanceled = false;//TicketFacade.ReturnTicket(new ReturnTicketDTO() { CompanyID = ?});
            if (canBeCanceled)
            {
                return View();
            }
            else
            {
                return RedirectToAction("Index");
            }
        }

        // POST: Ticket/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
