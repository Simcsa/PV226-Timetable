﻿using BL.Facades;
using System.Collections.Generic;
using System.Web.Mvc;
using BL.DTOs.Companies;
using BL.DTOs.Filters;
using PL.Models.Companies;
using BL.DTOs.Discounts;
using BL.Utils;

namespace PL.Controllers
{
    [Authorize(Roles = Claims.Admin)]
    public class CompanyController : Controller
    {
        #region Facades

        public CompanyFacade CompanyFacade { get; set; }

        #endregion

        public CompanyController(CompanyFacade companyFacade)
        {
            CompanyFacade = companyFacade;
        }

        // GET: Company
        [AllowAnonymous]
        public ActionResult Index(string name)
        {
            var companyFilter = new CompanyFilter()
            {
                Name = name,
            };

            IEnumerable<CompanyListDTO> listDtos = CompanyFacade.List(companyFilter);

            var companies = new CompanyListViewModel
            {
                Companies = listDtos,
            };

            return View("CompanyListView", companies);
        }

        // GET: Company/Details/5
        [AllowAnonymous]
        public ActionResult Details(int id)
        {
            var company = CompanyFacade.GetCompany(id);
            var catalogue = CompanyFacade.GetPriceCatalogue(company.PriceCatalogueId.Value);

            var companyDetails = new CompanyDetailViewModel
            {
                CompanyDTO = company,
                Discounts = catalogue.Discounts
            };

            return View("CompanyDetailView", companyDetails);
        }

        // GET: Company/CreateDiscount
        public ActionResult CreateDiscount(int priceCatalogueId)
        {
            return View("DiscountCreateView", 
                new DiscountViewModel
                {
                    DiscountDTO = new DiscountCreateDTO
                    {
                        PriceCatalogueId = priceCatalogueId
                    }
                });
        }

        // POST: Company/CreateDiscount
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateDiscount(DiscountViewModel discount)
        {
            CompanyFacade.AddPriceCatalogueDiscount(discount.DiscountDTO);
            return RedirectToAction("Index");
        }

        // GET: Company/EditDiscount
        public ActionResult EditDiscount(int priceCatalogueId, int discountId)
        {
            var catalogue = CompanyFacade.GetPriceCatalogue(priceCatalogueId);
            DiscountDetailDTO discountToEdit = null;

            foreach (DiscountDetailDTO discount in catalogue.Discounts)
            {
                if (discount.ID == discountId)
                {
                    discountToEdit = discount;
                    break;
                }
            }

            return View("DiscountEditView",
                new DiscountViewModel
                {
                    DiscountDTO = new DiscountCreateDTO
                    {
                        PriceCatalogueId = priceCatalogueId,
                        DiscountPercentage = discountToEdit.DiscountPercentage,
                        TicketType = discountToEdit.TicketType,
                        ID = discountId
                    }
                });
        }

        // POST: Company/EditDiscount
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditDiscount(DiscountViewModel discount)
        {
            CompanyFacade.EditPriceCatalogueDiscount(discount.DiscountDTO);
            return RedirectToAction("Index");
        }

        // GET: Company/DeleteDiscount
        public ActionResult DeleteDiscount(int priceCatalogueId, int discountId)
        {
            var catalogue = CompanyFacade.GetPriceCatalogue(priceCatalogueId);
            DiscountDetailDTO discountToDelete = null;

            foreach (DiscountDetailDTO discount in catalogue.Discounts)
            {
                if (discount.ID == discountId)
                {
                    discountToDelete = discount;
                    break;
                }
            }

            return View("DiscountDeleteView",
                new DiscountViewModel
                {
                    DiscountDTO = new DiscountCreateDTO
                    {
                        PriceCatalogueId = priceCatalogueId,
                        DiscountPercentage = discountToDelete.DiscountPercentage,
                        TicketType = discountToDelete.TicketType,
                        ID = discountId
                    }
                });
        }

        // POST: Company/DeleteDiscount
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteDiscount(DiscountViewModel discount)
        {
            CompanyFacade.RemovePriceCatalogueDiscount(new DiscountCreateDTO
            {
                PriceCatalogueId = discount.DiscountDTO.PriceCatalogueId,
                ID = discount.DiscountDTO.ID,
                DiscountPercentage = discount.DiscountDTO.DiscountPercentage,
                TicketType = discount.DiscountDTO.TicketType
            });

            return RedirectToAction("Index");
        }

        // GET: Company/Edit/5
        public ActionResult Edit(int id)
        {
            var company = CompanyFacade.GetCompany(id);
            return View("CompanyEditView", company);
        }

        // POST: Company/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(CompanyDetailDTO companyDto)
        {
            if (ModelState.IsValid)
            {
                CompanyFacade.UpdateCompany(companyDto);

                var catalogue = CompanyFacade.GetPriceCatalogue(companyDto.PriceCatalogueId.Value);

                var companyDetails = new CompanyDetailViewModel
                {
                    CompanyDTO = companyDto,
                    Discounts = catalogue.Discounts
                };

                return View("CompanyDetailView", companyDetails);
            }

            return RedirectToAction("Index");
        }
    }
}