﻿using System.Collections.Generic;
using System.Web.Mvc;
using BL.DTOs.Filters;
using BL.DTOs.Links;
using BL.Facades;
using PL.Models;
using System;
using BL.Utils;

namespace PL.Controllers
{
    [Authorize(Roles = Claims.Admin)]
    public class LinkController : Controller
    {

        public LinkFacade LinkFacade { get; set; }

        // GET: Link
        [AllowAnonymous]
        public ActionResult Index(string leaveStop, string arriveStop)
        {
            var filter = new LinkFilter()
            {
                LeaveStopName = leaveStop,
                ArriveStopName = arriveStop,
                LeaveTime = DateTime.Now
            };

            IEnumerable<LinkDTO> listDtos = LinkFacade.List(filter);

            var model = new LinkListViewModel()
            {
                Links = listDtos,
            };

            return View("LinkListView", model);
        }

        // GET: Link/Details/5
        [AllowAnonymous]
        public ActionResult Details(int id)
        {
            LinkDTO linkDto = LinkFacade.GetLink(id);
            return View("LinkDetailView", linkDto);
        }

        // GET: Link/EditDelay
        public ActionResult EditDelay(int id)
        {
            LinkDTO linkDto = LinkFacade.GetLink(id);
            return View("DelayEditView", linkDto);
        }

        // POST: Link/EditDelay
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditDelay(LinkDTO link)
        {
            LinkDTO linkToUpdate = LinkFacade.GetLink(link.ID);
            linkToUpdate.Delay = link.Delay;

            LinkFacade.UpdateLink(linkToUpdate);
            return RedirectToAction("Index");
        }
    }
}
