﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PL.Controllers
{
    [AllowAnonymous]
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            return View();
        }

        public ActionResult Company()
        {
            ViewBag.Message = "Companies";

            return RedirectToAction(nameof(Index));
        }

        public ActionResult Ticket()
        {
            ViewBag.Message = "Tickets";

            return RedirectToAction(nameof(Index));
        }
        public ActionResult Link()
        {
            ViewBag.Message = "Links";

            return RedirectToAction(nameof(Index));
        }
    }
}