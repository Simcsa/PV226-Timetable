﻿using BL.DTOs.Filters;
using BL.DTOs.Stops;
using BL.Facades;
using DAL.Entities;
using PL.Models;
using System.Collections.Generic;
using System.Web.Mvc;

namespace PL.Controllers
{
    [Authorize]
    public class StopController : Controller
    {
        public StopFacade StopFacade { get; set; }

        // GET: Stop
        [AllowAnonymous]
        public ActionResult Index(string name, string town)
        {
            var filter = new StopFilter()
            {
                Name = name,
                Town = town
            };

            IEnumerable<StopDTO> stopDtos = StopFacade.List(filter);

            var model = new StopListViewModel()
            {
                Stops = stopDtos
            };

            return View("StopListView", model);
        }

        // GET: Stop/Details/5
        [AllowAnonymous]
        public ActionResult Details(int id)
        {
            StopDTO stopDto = StopFacade.Get(id);
            List<int> photos = StopFacade.GetPhotoIds(id);

            var model = new StopDetailViewModel()
            {
                StopDto = stopDto,
                PhotoIds = photos
            };

            return View("StopDetailView", model);
        }

        [AllowAnonymous]
        public ActionResult GetImage(int id)
        {
            Photo photo = StopFacade.GetPhoto(id);
            return File(photo.Data, "image/png");
        }

        // GET: Stop/AddPhoto
        public ActionResult AddPhoto(int stopId)
        {
            return View("AddPhotoView", new StopPhotoAddModel() { StopId = stopId });
        }

        // POST: Stop/AddPhoto
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddPhoto(StopPhotoAddModel photo)
        {
            var stop = new StopDTOWihPhotos() { ID = photo.StopId };

            if (photo != null)
            {
                using (var reader = new System.IO.BinaryReader(photo.File.InputStream))
                {
                    var image = reader.ReadBytes(photo.File.ContentLength);
                    List<Photo> images = new List<Photo>();
                    images.Add(new Photo() { Data = image });
                    stop.Photos = images;
                }
                StopFacade.AddPhotos(stop);
            }
            return RedirectToAction("Index");
        }
    }
}