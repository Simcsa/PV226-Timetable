﻿using System.Collections.Generic;
using BL.DTOs.Tickets;

namespace PL.Models
{
    public class TicketListViewModel
    {
        public IEnumerable<TicketDTO> Tickets { get; set; }
    }
}