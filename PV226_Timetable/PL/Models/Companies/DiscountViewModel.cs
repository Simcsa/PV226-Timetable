﻿using BL.DTOs.Discounts;

namespace PL.Models.Companies
{
    public class DiscountViewModel
    {
        public DiscountCreateDTO DiscountDTO { get; set; }
    }
}