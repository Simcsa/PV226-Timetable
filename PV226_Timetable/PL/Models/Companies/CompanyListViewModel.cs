﻿using System.Collections.Generic;

using BL.DTOs.Companies;

namespace PL.Models.Companies
{
    public class CompanyListViewModel
    {
        public IEnumerable<CompanyListDTO> Companies { get; set; }
    }
}