﻿using BL.DTOs.Companies;
using BL.DTOs.Discounts;
using System.Collections.Generic;

namespace PL.Models.Companies
{
    public class CompanyDetailViewModel
    {
        public CompanyDetailDTO CompanyDTO { get; set; }

        public HashSet<DiscountDetailDTO> Discounts { get; set; }
    }
}