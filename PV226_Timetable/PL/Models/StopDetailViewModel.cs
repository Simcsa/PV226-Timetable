﻿using BL.DTOs.Stops;
using DAL.Entities;
using System.Collections.Generic;

namespace PL.Models
{
    public class StopDetailViewModel
    {
        public StopDTO StopDto { get; set; }

        public List<int> PhotoIds { get; set; }
    }
}