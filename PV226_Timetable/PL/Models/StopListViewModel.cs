﻿using BL.DTOs.Stops;
using System.Collections.Generic;

namespace PL.Models
{
    public class StopListViewModel
    {
        public IEnumerable<StopDTO> Stops { get; set; }
    }
}