﻿using System.Web;

namespace PL.Models
{
    public class StopPhotoAddModel
    {
        public int StopId { get; set; }

        public HttpPostedFileBase File { get; set; }
    }
}