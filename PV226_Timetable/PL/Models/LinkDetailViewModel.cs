﻿using BL.DTOs.Links;
using System;
using System.Collections.Generic;

namespace PL.Models
{
    public class LinkDetailViewModel
    {
        public LinkDTO LinkDTO { get; set; }

        public List<DateTime> LeaveTimes { get; set; }
    }
}