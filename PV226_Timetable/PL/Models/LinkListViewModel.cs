﻿using System.Collections.Generic;
using BL.DTOs.Links;

namespace PL.Models
{
    public class LinkListViewModel
    {
        public IEnumerable<LinkDTO> Links { get; set; }
    }
}