﻿using System;
using System.Collections.Generic;
using System.Linq;
using BL.AppInfrastructure;
using BL.DTOs.Companies;
using BL.DTOs.Filters;
using BL.DTOs.Forms;
using BL.DTOs.Links;
using BL.DTOs.Routes;
using BL.DTOs.Seats;
using BL.DTOs.Stops;
using BL.DTOs.Tickets;
using BL.DTOs.TimedStops;
using BL.DTOs.TwoStopRoutes;
using BL.DTOs.Users;
using BL.DTOs.Vehicles;
using BL.Services.Companies;
using BL.Services.Forms;
using BL.Services.Links;
using BL.Services.Routes;
using BL.Services.Seats;
using BL.Services.Stops;
using BL.Services.Tickets;
using BL.Services.TimedStops;
using BL.Services.TwoStopRoutes;
using BL.Services.Users;
using BL.Services.Vehicles;
using Castle.Windsor;
using DAL.Entities;
using BL.Services.PriceCatalogues;
using BL.DTOs.Discounts;
using BL.DTOs.PriceCatalogues;
using BL.Utils;

namespace ConsoleApplication
{
    public class Program
    {
        private static readonly IWindsorContainer Container = new WindsorContainer();

        #region Service Variables

        private static ICompanyService companyService;
        private static IPriceCatalogueService priceCatalogueService;
        private static IFormService formService;
        private static ILinkService linkService;
        private static ITicketService ticketService;
        private static IStopService stopService;
        private static IUserService userService;
        private static ISeatService seatService;
        private static IRouteService routeService;
        private static IVehicleService vehicleService;
        private static ITimedStopService timedStopService;
        private static ITwoStopRouteService twoStopService;

        #endregion

        #region Entity Ids Variables

        private static int companyId;
        private static int userId;
        private static int vehicleId;
        private static int ticketId;

        private static int stopIdAntoninska;
        private static int stopIdPionyrska;
        private static int stopIdHrncirska;
        private static int stopIdSumavska;

        private static int timedStopIdAntoninska;
        private static int timedStopIdPionyrska;
        private static int timedStopIdHrncirska;
        private static int timedStopIdSumavska;

        private static int twoStopRouteIdAntPio;
        private static int twoStopRouteIdPioHrnc;
        private static int twoStopRouteIdHrncSum;

        private static int routeIdAntSum;
        private static int linkId;

        #endregion

        #region Other Variables

        private static DateTime linkLeaveTime = new DateTime(2016, 11, 24, 17, 35, 0);
        private static int vehicleCapacity = 30;
        private static int numOfOccupiedSeats = 0;

        #endregion

        #region Main

        static void Main(string[] args)
        {
            InitializeWindsorContainerAndMapper();

            SetUpTestData();

            TestCompanyService();
            TestPriceCatalogueService();
            TestVehicleService();
            TestUserService();
            TestStopService();
            TestLinkService();
            TestSeatService();
            TestTicketService();
            TestFormService();

            Console.ReadLine();
        }

        #endregion

        #region Initialize container and mapper

        private static void InitializeWindsorContainerAndMapper()
        {
            Container.Install(new BusinessLayerInstaller());
            companyService = Container.Resolve<ICompanyService>();
            priceCatalogueService = Container.Resolve<IPriceCatalogueService>();
            userService = Container.Resolve<IUserService>();
            linkService = Container.Resolve<ILinkService>();
            seatService = Container.Resolve<ISeatService>();
            stopService = Container.Resolve<IStopService>();
            ticketService = Container.Resolve<ITicketService>();
            routeService = Container.Resolve<IRouteService>();
            vehicleService = Container.Resolve<IVehicleService>();
            timedStopService = Container.Resolve<ITimedStopService>();
            twoStopService = Container.Resolve<ITwoStopRouteService>();
            formService = Container.Resolve<IFormService>();

            MapperInitializer.ConfigureMapping();
        }

        #endregion

        #region Set up test data

        private static void SetUpTestData()
        {
            SetUpCompaniesAndVehicles();
            SetUpUsers();
            SetUpStops();
            SetUpLinks();
            SetUpTickets();
        }

        private static void SetUpCompaniesAndVehicles()
        {
            // Companies
            CompanyDetailDTO companyDto = new CompanyDetailDTO()
            {
                Name = "Company",
                PricePerKm = 2,
            };
            companyId = companyService.Create(companyDto);

            // Vehicles
            VehicleDTOCreate vehicleDTO = new VehicleDTOCreate()
            {
                Capacity = vehicleCapacity,
                CompanyId = companyId,
                VehicleType = VehicleType.Tram
            };
            vehicleId = vehicleService.Create(vehicleDTO);
        }

        private static void SetUpUsers()
        {
            UserDTO userDTO = new UserDTO()
            {
                FirstName = "Harry",
                LastName = "Pottah",
                Email = "hpottah@gmail.com"
            };
            userId = userService.Create(userDTO);
        }

        private static void SetUpStops()
        {
            StopDTO antoninskaStopId = new StopDTO()
            {
                Name = "Antoninska",
                Town = "Brno",
            };
            stopIdAntoninska = stopService.Create(antoninskaStopId);

            StopDTO pionyrskaStopDTO = new StopDTO()
            {
                Name = "Pionyrska",
                Town = "Brno",
            };
            stopIdPionyrska = stopService.Create(pionyrskaStopDTO);

            StopDTO hrncirskaStopDTO = new StopDTO()
            {
                Name = "Hrncirska",
                Town = "Brno",
            };
            stopIdHrncirska = stopService.Create(hrncirskaStopDTO);

            StopDTO sumavskaStopDTO = new StopDTO()
            {
                Name = "Sumavska",
                Town = "Brno",
            };
            stopIdSumavska = stopService.Create(sumavskaStopDTO);
        }

        private static void SetUpLinks()
        {
            // Timed stops
            TimedStopDTO antoninskaTimedStopDTO = new TimedStopDTO()
            {
                StopId = stopIdAntoninska,
                StopTime = new TimeSpan(0, 1, 30),
            };
            timedStopIdAntoninska = timedStopService.Create(antoninskaTimedStopDTO);

            TimedStopDTO pionyrskaTimedStopDTO = new TimedStopDTO()
            {
                StopId = stopIdPionyrska,
                StopTime = new TimeSpan(0, 1, 40),
            };
            timedStopIdPionyrska = timedStopService.Create(pionyrskaTimedStopDTO);

            TimedStopDTO hrncirskaTimedStopDTO = new TimedStopDTO()
            {
                StopId = stopIdHrncirska,
                StopTime = new TimeSpan(0, 1, 0),
            };
            timedStopIdHrncirska = timedStopService.Create(hrncirskaTimedStopDTO);

            TimedStopDTO sumavskaTimedStopDTO = new TimedStopDTO()
            {
                StopId = stopIdSumavska,
                StopTime = new TimeSpan(0, 0, 50),
            };
            timedStopIdSumavska = timedStopService.Create(sumavskaTimedStopDTO);

            // Routes
            RouteDTO routeDTO = new RouteDTO();
            routeIdAntSum = routeService.Create(routeDTO);

            // Two stop routes
            TwoStopRouteDTO AntPioTwoStopRouteDTO = new TwoStopRouteDTO()
            {
                LeaveStopId = timedStopIdAntoninska,
                ArriveStopId = timedStopIdPionyrska,
                RouteId = routeIdAntSum,
                LengthInKm = 1.2m,
                TravelTime = new TimeSpan(0, 1, 30),
                Order = 1
            };
            twoStopRouteIdAntPio = twoStopService.Create(AntPioTwoStopRouteDTO);

            TwoStopRouteDTO PioHrncTwoStopRouteDTO = new TwoStopRouteDTO()
            {
                LeaveStopId = timedStopIdPionyrska,
                ArriveStopId = timedStopIdHrncirska,
                RouteId = routeIdAntSum,
                LengthInKm = 1.1m,
                TravelTime = new TimeSpan(0, 1, 5),
                Order = 2
            };
            twoStopRouteIdPioHrnc = twoStopService.Create(PioHrncTwoStopRouteDTO);

            TwoStopRouteDTO hrncSumTwoStopRouteDTO = new TwoStopRouteDTO()
            {
                LeaveStopId = timedStopIdHrncirska,
                ArriveStopId = timedStopIdSumavska,
                RouteId = routeIdAntSum,
                LengthInKm = 1.3m,
                TravelTime = new TimeSpan(0, 0, 30),
                Order = 3
            };
            twoStopRouteIdHrncSum = twoStopService.Create(hrncSumTwoStopRouteDTO);

            // Links
            LinkDTO linkDTO = new LinkDTO()
            {
                Delay = TimeSpan.Zero,
                LeaveTime = linkLeaveTime,
                RouteID = routeIdAntSum,
                VehicleID = vehicleId,
            };
            linkId = linkService.Create(linkDTO);

            // Seats
            SeatDTO seatDTO;
            for (int i = 1; i <= vehicleCapacity; i++)
            {
                seatDTO = new SeatDTO()
                {
                    Number = i,
                    LinkID = linkId
                };
                seatService.Create(seatDTO);
            }
        }

        private static void SetUpTickets()
        {
            // Tickets
            TicketDTOCreate ticketDTO = new TicketDTOCreate()
            {
                FromStopID = stopIdHrncirska,
                ToStopID = stopIdSumavska,
                IsPayed = false,
                Price = 100,
                SeatIDs = new List<int>() { seatService.FindEmptySeats(linkId).First().ID },
                UserId = userId
            };
            ticketId = ticketService.Create(ticketDTO);
            numOfOccupiedSeats++;
        }

        #endregion

        #region Test services

        private static void TestCompanyService()
        {
            // Create
            var companyName = "TestCompany";

            CompanyDetailDTO companyDto = 
                new CompanyDetailDTO() { Name = companyName, PricePerKm = 2 };

            var companyId = companyService.Create(companyDto);
            var companyId2 = companyService.List(new CompanyFilter() { Name = companyName }).First().ID;

            Console.WriteLine(companyId == companyId2 ? "Company id retrieve - OK" : "Company id retrieve - FAIL");

            var dto = companyService.Get(companyId);

            var companyNameChanged = "NewNameCompany";

            // Update
            companyService.Update(new CompanyDetailDTO()
            {
                ID = companyId,
                Name = companyNameChanged,
                PriceCatalogueId = dto.PriceCatalogueId
            });

            dto = companyService.Get(companyId);
            Console.WriteLine(dto.Name == companyNameChanged ? "Company update - OK" : "Company update - FAIL");
            
            // Delete
            companyService.Delete(companyId);

            Console.WriteLine((companyService.Get(companyId) == null) ? "Company delete - OK" : "Company delete - FAIL");

            Console.WriteLine();
        }

        private static void TestPriceCatalogueService()
        {
            CompanyDetailDTO company = companyService.Get(companyId);
            int priceCatalogueId = company.PriceCatalogueId.Value;

            DiscountCreateDTO discountDTOStudent = new DiscountCreateDTO()
            {
                DiscountPercentage = 25,
                TicketType = TicketType.Student,
                PriceCatalogueId = priceCatalogueId
            };

            DiscountCreateDTO discountDTOChild = new DiscountCreateDTO()
            {
                DiscountPercentage = 50,
                TicketType = TicketType.Child,
                PriceCatalogueId = priceCatalogueId
            };

            bool discountCreated = priceCatalogueService.AddDiscount(discountDTOStudent);
            PriceCatalogueDTO priceCatalogue = priceCatalogueService.Get(priceCatalogueId);
            Console.WriteLine(discountCreated && priceCatalogue.Discounts != null && priceCatalogue.Discounts.Count == 1 ? "Student discount create - OK" : "Student discount create - FAIL");

            discountDTOStudent.DiscountPercentage = 30;
            bool discountUpdated = priceCatalogueService.UpdateDiscount(discountDTOStudent);
            priceCatalogue = priceCatalogueService.Get(priceCatalogueId);
            Console.WriteLine(discountUpdated && priceCatalogue.Discounts.FirstOrDefault().DiscountPercentage == discountDTOStudent.DiscountPercentage ? "Student discount update - OK" : "Student discount update - FAIL");

            discountCreated = priceCatalogueService.AddDiscount(discountDTOChild);
            priceCatalogue = priceCatalogueService.Get(priceCatalogueId);
            Console.WriteLine(discountCreated && priceCatalogue.Discounts != null && priceCatalogue.Discounts.Count == 2 ? "Child discount create - OK" : "Child discount create - FAIL");

            discountCreated = priceCatalogueService.AddDiscount(discountDTOChild);
            priceCatalogue = priceCatalogueService.Get(priceCatalogueId);
            Console.WriteLine(!discountCreated && priceCatalogue.Discounts.Count == 2 ? "Child discount not created twice - OK" : "Child discount created twice - FAIL");
            
            bool discountRemoved = priceCatalogueService.RemoveDiscount(discountDTOStudent);
            priceCatalogue = priceCatalogueService.Get(priceCatalogueId);
            Console.WriteLine(discountRemoved && priceCatalogue.Discounts.Count == 1 ? "Student discount delete - OK" : "Student discount delete - FAIL");

            Console.WriteLine();
        }

        private static void TestVehicleService()
        {
            VehicleDTOCreate vehicleDTO = new VehicleDTOCreate()
            {
                Capacity = 200,
                CompanyId = companyId,
                VehicleType = VehicleType.Train
            };
            int trainId = vehicleService.Create(vehicleDTO);

            VehicleDTO vehicle = vehicleService.Get(trainId);

            Console.WriteLine(trainId == vehicle.ID ? "Vehicle id retrieve - OK" : "Vehicle id retrieve - FAIL");

            var vehicleFilter = new VehicleFilter();
            IEnumerable<VehicleDTO> vehicles = vehicleService.List(vehicleFilter);
            Console.WriteLine(vehicles.Count() == 3 ? "Vehicle list - OK" : "Vehicle list - FAIL");

            vehicleDTO.Capacity = 22;
            vehicleDTO.ID = trainId;
            vehicleService.Update(vehicleDTO);

            vehicleFilter.Capacity = new Range<int>() { Minimum = 20, Maximum = 23 };
            vehicles = vehicleService.List(vehicleFilter);
            Console.WriteLine(vehicles.Count() == 1 ? "Vehicle update and list by capacity - OK" : "Vehicle update and list by capacity - FAIL");

            vehicleFilter.Capacity = new Range<int>() { Minimum = 5, Maximum = 200 };
            vehicleFilter.VehicleTypes = new HashSet<VehicleType>() { VehicleType.Train };
            vehicles = vehicleService.List(vehicleFilter);
            Console.WriteLine(vehicles.Count() == 1 ? "Vehicle list by vehicle type - OK" : "Vehicle list by vehicle type - FAIL");

            vehicleService.Delete(vehicleDTO.ID);
            vehicles = vehicleService.List(vehicleFilter);
            Console.WriteLine(vehicles.Count() == 0 ? "Vehicle delete - OK" : "Vehicle delete - FAIL");

            Console.WriteLine();
        }

        private static void TestUserService()
        {
            UserDTO userDTO = new UserDTO()
            {
                FirstName = "Harry",
                LastName = "Potter",
                Email = "hpotter@gmail.com"
            };
            int harryId = userService.Create(userDTO);
            UserDTO harry = userService.Get(harryId);

            Console.WriteLine(harryId == harry.ID ? "User id retrieve - OK" : "User id retrieve - FAIL");

            var userFilter = new UserFilter();
            IEnumerable<UserDTO> users = userService.List(userFilter);
            Console.WriteLine(users.Count() == 3 ? "User list - OK" : "User list - FAIL");

            userDTO.LastName = "Potterik";
            userDTO.ID = harryId;
            userService.Update(userDTO);

            userFilter.LastName = userDTO.LastName;
            users = userService.List(userFilter);
            Console.WriteLine(users.Count() == 1 ? "User update and list by last name - OK" : "User update and list by last name - FAIL");

            userService.Delete(userDTO.ID);
            users = userService.List(userFilter);
            Console.WriteLine(users.Count() == 0 ? "User delete - OK" : "User delete - FAIL");

            Console.WriteLine();
        }

        private static void TestStopService()
        {
            var stopFilter = new StopFilter() { Name = "Hrncirska" };
            IEnumerable<StopDTO> stops = stopService.List(stopFilter);
            Console.WriteLine(stops.Count() == 1 ? "Stop list by name - OK" : "Stop list by name - FAIL");

            stopFilter.Name = null;
            stopFilter.Town = "Brno";
            stops = stopService.List(stopFilter);
            Console.WriteLine(stops.Count() == 7 ? "Stop list by town - OK" : "Stop list by town - FAIL");

            List<Photo> photos = new List<Photo>();
            photos.Add(new Photo() { Data = new byte[2] });

            StopDTOWihPhotos stopDto = new StopDTOWihPhotos()
            {
                ID = stopIdHrncirska,
                Photos = photos
            };

            stopService.AddPhotos(stopDto);

            var getPhotos = stopService.GetPhotoIds(stopDto.ID);

            Console.WriteLine(photos.Count() == getPhotos.Count() ? "Stop add photos - OK" : "Stop add photos - FAIL");

            Console.WriteLine();
        }

        private static void TestLinkService()
        {
            var linkFilter = new LinkFilter() { LeaveTime = linkLeaveTime, LeaveStopName = "Hrncirska", ArriveStopName = "Sumavska"};
            IEnumerable<LinkDTO> links = linkService.List(linkFilter);
            Console.WriteLine(links.Count() == 1 ? "Link list - OK" : "Link list - FAIL");

            linkFilter = new LinkFilter() { LeaveTime = linkLeaveTime, LeaveStopName = "Hrncirska", ArriveStopName = "Pionyrska" };
            links = linkService.List(linkFilter);
            Console.WriteLine(links.Count() == 0 ? "Link does not exist list - OK" : "Link does not exist list - FAIL");

            Console.WriteLine();
        }

        private static void TestSeatService()
        {
            IEnumerable<SeatDTO> seats = seatService.List(new SeatFilter() { LinkId = linkId });
            Console.WriteLine(seats.Count() == vehicleCapacity ? "List seats - OK" : "List seats - FAIL");

            seats = seatService.FindEmptySeats(linkId);
            Console.WriteLine(seats.Count() == vehicleCapacity - numOfOccupiedSeats ? "Find empty seats - OK" : "Find empty seats - FAIL");

            Console.WriteLine();
        }

        private static void TestTicketService()
        {
            var emptySeats = seatService.FindEmptySeats(linkId);
            var newSeatId = emptySeats.First().ID;
            var newSeatId1 = emptySeats.Last().ID;
            var price = 22;

            var seatIds = new List<int>() { newSeatId, newSeatId1 };

            TicketDTOCreate ticketDTO = new TicketDTOCreate()
            {
                FromStopID = stopIdHrncirska,
                ToStopID = stopIdSumavska,
                IsPayed = false,
                Price = price,
                SeatIDs = seatIds,
                UserId = userId
            };

            ticketService.ReserveTickets(new List<TicketDTOCreate>() { ticketDTO });

            IEnumerable<TicketDTO> tickets = ticketService.List(new TicketFilter() { Price = new Range<int>() { Minimum = 20, Maximum = 23 } });
            Console.WriteLine(tickets.Count() == 2 ? "Reserve tickets - OK" : "Reserve tickets - FAIL");




            //Console.WriteLine(result ? "BuyTicket - OK" : "BuyTicket - FAIL");

            //ReturnTicketDTO returnTicketDTO = new ReturnTicketDTO()
            //{
            //    CompanyID = companyId,
            //};

            //result = ticketService.ReturnTicket(returnTicketDTO);
            //Console.WriteLine(result ? "ReturnTicket - FAIL" : "ReturnTicket - OK");

            //ticketService.SendEmail(ticketDTO);

            Console.WriteLine();
        }

        private static void TestFormService()
        {
            FormDTOCreate formDTO = new FormDTOCreate()
            {
                Rating = 6,
                Comment = "Train could be faster!",
                TicketID = ticketId
            };
            int formId = formService.Create(formDTO);

            IEnumerable<FormDTO> formDtos = formService.List(new FormFilter()
            {
                Rating = new Range<int>() { Minimum = 5, Maximum = 6 }
            });

            Console.WriteLine(formDtos.Count() == 1 ? "Form get by rating - OK" : "Form get by rating - FAIL");
        }

        #endregion
    }
}
