﻿using BL.DTOs.Filters;
using BL.Facades;
using System.Net;
using System.Web.Http;

namespace WebAPI.Controllers
{
    public class LinkController : ApiController
    {
        public LinkFacade LinkFacade { get; set; }

        /// <summary>
        /// GET: api/Links
        /// Gets all links
        /// </summary>
        /// <returns>HTTP 200 on success</returns>
        public IHttpActionResult Get()
        {
            return Content(HttpStatusCode.OK, LinkFacade.List(new LinkFilter() { }));
        }

        /// <summary>
        /// GET: api/Link/1
        /// Gets link details.
        /// </summary>
        /// <param name="id">id of link</param>
        /// <returns>HTTP 200 on success</returns>
        [Route("~/api/Link/{id}")]
        public IHttpActionResult Get(int id)
        {
            var result = id <= 0 ? null : LinkFacade.GetLink(id);
            return result == null
                ? (IHttpActionResult)NotFound()
                : Content(HttpStatusCode.OK, result);
        }
    }
}