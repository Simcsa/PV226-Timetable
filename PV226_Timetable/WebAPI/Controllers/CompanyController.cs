﻿using BL.DTOs.Companies;
using BL.DTOs.Filters;
using BL.Facades;
using Newtonsoft.Json;
using System;
using System.Diagnostics;
using System.Net;
using System.Web.Http;

namespace WebAPI.Controllers
{
    public class CompanyController : ApiController
    {
        public CompanyFacade CompanyFacade { get; set; }

        /// <summary>
        /// GET: api/Companies
        /// Gets all companies
        /// </summary>
        /// <returns>HTTP 200 on success</returns>
        public IHttpActionResult Get()
        {
            return Content(HttpStatusCode.OK, CompanyFacade.List(new CompanyFilter() { }));
        }

        /// <summary>
        /// GET: api/Company/1
        /// Gets company details.
        /// </summary>
        /// <param name="id">id of company</param>
        /// <returns>HTTP 200 on success</returns>
        [Route("~/api/Company/{id}")]
        public IHttpActionResult Get(int id)
        {
            var result = id <= 0 ? null : CompanyFacade.GetCompany(id);
            return result == null
                ? (IHttpActionResult)NotFound()
                : Content(HttpStatusCode.OK, result);
        }

        /// <summary>
        /// PUT: api/Company/1
        /// Updates corresponding company
        /// </summary>
        /// <param name="id">id of company to update</param>
        /// <param name="value">modified company data</param>
        /// <returns>HTTP 200 on success</returns>
        public IHttpActionResult Put(int id, [FromBody]string value)
        {
            try
            {
                var company = JsonConvert.DeserializeObject<CompanyDetailDTO>(value);
                if (id <= 0)
                {
                    return Content(HttpStatusCode.PreconditionFailed, "Company ID must be greater than zero.");
                }
                company.ID = id;
                CompanyFacade.UpdateCompany(company);
                return Content(HttpStatusCode.OK, company);
            }
            catch (JsonException)
            {
                Debug.WriteLine($"Company API - Put(...) - failed to deserialize value: {value}");
                return StatusCode(HttpStatusCode.BadRequest);
            }
            catch (NullReferenceException ex)
            {
                Debug.WriteLine(ex.Message);
                return StatusCode(HttpStatusCode.NotFound);
            }

        }
    }
}
